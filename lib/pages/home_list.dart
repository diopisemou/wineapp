import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:wineapp/bloc/category/category_bloc.dart';
import 'package:wineapp/bloc/place/place_bloc.dart';
import 'package:wineapp/bloc/shared.dart';
import 'package:wineapp/models/category.dart';
import 'package:wineapp/models/place.dart';
import 'package:wineapp/pages/details.dart';
import 'package:wineapp/pages/place_info_screen.dart';
import 'package:wineapp/utils/app_theme.dart';
import 'package:wineapp/utils/extension.dart';
import 'package:wineapp/utils/places.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

class HomeList extends StatefulWidget {
  const HomeList({Key key, this.callBack}) : super(key: key);

  final Function callBack;
  @override
  _HomeListState createState() => _HomeListState();
}

class _HomeListState extends State<HomeList> with TickerProviderStateMixin {
  final TextEditingController _searchControl = new TextEditingController();
  AnimationController animationController;

  bool multipleDelete = false;
  List<Category> _listToDelete = List<Category>();

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));

    context.read<CategoryBloc>()..add(LoadFavoriteCategories());
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  Future<bool> buildhowToast(AfterFavoriteCategoriesUpdated state) {
    return Fluttertoast.showToast(
      msg: state.message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: state.updateStatus == UpdateStatus.success
          ? Colors.blue
          : Colors.yellow,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(55.0),
                ),
              ),
              child: TypeAheadField<Place>(
                noItemsFoundBuilder: (context) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Aucun endroit correspondant",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).disabledColor, fontSize: 18.0),
                  ),
                ),
                textFieldConfiguration: TextFieldConfiguration(
                  maxLines: 1,
                  autofocus: false,
                  style: DefaultTextStyle.of(context)
                      .style
                      .copyWith(fontStyle: FontStyle.italic),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(55.0),
                      borderSide: BorderSide(
                        color: Colors.blueGrey[300],
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                      borderRadius: BorderRadius.circular(55.0),
                    ),
                    hintText: "E.g: New York, United States",
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.blueGrey[300],
                    ),
                    hintStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.blueGrey[300],
                    ),
                  ),
                ),
                suggestionsCallback: (pattern) async {
                  return await context
                      .read<PlaceBloc>()
                      .getPlaceSuggestion(pattern);
                },
                itemBuilder: (context, sugestedPlace) {
                  return ListTile(
                    leading: Icon(Icons.location_on),
                    title: Text(sugestedPlace.name),
                    subtitle: Text('\$${sugestedPlace.price}'),
                  );
                },
                onSuggestionSelected: (selectedPlace) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => Details(place: selectedPlace),
                    ),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Avis d\'autres utilisateurs  ',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      letterSpacing: 0.27,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Row(
                    children: <Widget>[
                      Text(
                        ' Ajouter ',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          letterSpacing: 0.27,
                          color: AppTheme.darkerText,
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.blue[600],
                            borderRadius:
                                BorderRadius.all(Radius.circular(16.0)),
                          ),
                          child: Center(
                            child: Icon(
                              Icons.add,
                              color: AppTheme.white,
                              size: 23,
                            ),
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, left: 20),
            height: 100,
            //            color: Colors.red,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              primary: false,
              itemCount: places2 == null ? 0 : places2.length,
              itemBuilder: (BuildContext context, int index) {
                final place = places2.reversed.toList()[index];
                return Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: InkWell(
                    child: Container(
                      height: 100,
                      width: 80,
                      //                      color: Colors.green,
                      child: Column(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.circular(80),
                            child: Image.asset(
                              "${place.img.elementAt(Random().nextInt(place.img.length))}",
                              height: 55,
                              width: 55,
                              fit: BoxFit.cover,
                            ),
                          ),
                          SizedBox(height: 3),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "${place.name}",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                              ),
                              maxLines: 2,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return Details(place: place);
                          },
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
          MultiDeleteRow(),
          /** Les categories */
          Container(
            padding: EdgeInsets.only(top: 10, left: 20),
            height: 140,
            width: MediaQuery.of(context).size.width,
            child: BlocConsumer<CategoryBloc, CategoryState>(
                listener: (_, state) {
                  if (state is AfterFavoriteCategoriesUpdated) {
                    Fluttertoast.showToast(
                      msg: state.message,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor:
                          state.updateStatus == UpdateStatus.success
                              ? Colors.blue
                              : Colors.yellow,
                      textColor: Colors.white,
                      fontSize: 16.0,
                    );
                  }
                },
                //buildWhen: (context, state) => !(state is CategoriesDeleted),
                buildWhen: (context, state) =>
                    (state is FavoriteCategoriesLoaded),
                builder: (context, state) {
                  return ListView(
                    scrollDirection: Axis.horizontal,
                    primary: false,
                    children: [
                      InkWell(
                        onTap: () {
                          //  add new favorite category
                          _showModal();
                        },
                        child: Container(
                          height: 140,
                          width: 90,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                          ),
                          child: Center(
                            child: Icon(Icons.add, size: 50),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      if (state is FavoriteCategoriesLoaded)
                        ...(state.favoritecategoriesForChoice
                            .mapIndexed((e, index) {
                          return CategoryItem(
                            key: Key(e.category.key),
                            category: e.category,
                            selected: !e.isFavorite,
                            onLongPress: () {
                              context
                                  .read<CategoryBloc>()
                                  .add(LongPressFavoriteCategory(
                                    favoritecategoriesForChoice:
                                        state.favoritecategoriesForChoice,
                                    pressedFavorite:
                                        e.copyWith(isFavorite: !e.isFavorite),
                                  ));
                            },
                          );
                        }).toList())
                    ],
                  );
                }),
          ),
          SizedBox(height: 15),
          Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Lieux Trouvés ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                      letterSpacing: 0.27,
                      color: AppTheme.darkerText,
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Voir Tout ',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                          letterSpacing: 0.27,
                          color: AppTheme.darkerText,
                        ),
                      ),
                      SizedBox(width: 10),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.blue[600],
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16.0),
                          ),
                        ),
                        child: Icon(
                          Icons.add,
                          color: AppTheme.white,
                          size: 23,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          /** Fin lieux trouvés */
          Padding(
            padding: EdgeInsets.all(20),
            child: BlocBuilder<PlaceBloc, PlaceState>(buildWhen: (_, state) {
              return (state is PlaceForCategoryLoaded || state is PlaceLoaded);
            }, builder: (context, state) {
              return ListView(
                primary: false,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                children: [
                  if ((state is PlaceForCategoryLoaded))
                    ...(state.places
                        .map((place) => PlaceTile(place: place))
                        .toList()),
                  if (state is PlaceLoaded)
                    ...(state.places
                        .map((place) => PlaceTile(place: place))
                        .toList()),
                  SizedBox(height: 200)
                ],
              );
            }),
          )
        ],
      ),
    );
  }

  void _showModal() {
    context.read<CategoryBloc>().add(LoadCategoriesForChoice());
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (_) {
        return BlocProvider.value(
          value: context.read<CategoryBloc>(),
          child: FavoriteCategoryModalChild(),
        );
      },
    );
  }

  //   Widget getPopularCourseUI() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Text(
  //           'Popular Course',
  //           textAlign: TextAlign.left,
  //           style: TextStyle(
  //             fontWeight: FontWeight.w600,
  //             fontSize: 22,
  //             letterSpacing: 0.27,
  //             color: DesignCourseAppTheme.darkerText,
  //           ),
  //         ),
  //         Flexible(
  //           child: PopularCourseListView(
  //             callBack: () {
  //               moveTo();
  //             },
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

}

class FavoriteCategoryModalChild extends StatefulWidget {
  const FavoriteCategoryModalChild({
    Key key,
  }) : super(key: key);

  @override
  _FavoriteCategoryModalChildState createState() =>
      _FavoriteCategoryModalChildState();
}

class _FavoriteCategoryModalChildState
    extends State<FavoriteCategoryModalChild> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 20),
        Center(child: Container(width: 50, height: 4, color: Colors.grey)),
        SizedBox(height: 15),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Text("taper sur un category pour l'ajouter à votre category"),
        ),
        BlocBuilder<CategoryBloc, CategoryState>(
          buildWhen: (prev, next) => next is CategoryLoadedForChoiceFavorite,
          builder: (__, state) {
            if (state is CategoryLoadedForChoiceFavorite) {
              final categoriesFor = state.categoriesForChooseFavorite;
              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Wrap(
                    children: [
                      ...state.favoriteCategoriesForChoice.map((e) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 200),
                          child: Stack(
                            key: Key('tage${e.category.key}'),
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                margin: EdgeInsets.all(8.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(e.category.name,
                                      style: TextStyle(color: Colors.white)),
                                ),
                              ),
                              Positioned(
                                top: 0,
                                right: 0,
                                child: InkWell(
                                  onTap: () {
                                    context.read<CategoryBloc>().add(
                                          ToogleFavorite(
                                              categoriesForChoose:
                                                  categoriesFor,
                                              categoryForChooseFavorite: e
                                                  .copyWith(isFavorite: false)),
                                        );
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Icon(
                                      Icons.close,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      })
                    ],
                  ),
                  Container(
                    height: 100,
                    alignment: Alignment.centerLeft,
                    child: ListView.builder(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      scrollDirection: Axis.horizontal,
                      itemCount: state.categories.length,
                      itemBuilder: (_, index) {
                        final categoryForFavorite =
                            categoriesFor.elementAt(index);
                        return CategoryItemToFavorite(
                          key: Key(
                              'categoryForChoose${categoryForFavorite.category.key}'),
                          categoryForChoose: categoryForFavorite,
                          onTap: (item) {
                            context.read<CategoryBloc>().add(ToogleFavorite(
                                  categoriesForChoose:
                                      state.categoriesForChooseFavorite,
                                  categoryForChooseFavorite: item,
                                ));
                          },
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      ),
                      onPressed: () {
                        context.read<CategoryBloc>().add(
                            UpdateFavoriteCategories(state.favoriteCategories));
                      },
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Color(0xffF5D020), Color(0xffF53803)],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          borderRadius: BorderRadius.circular(8.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.orange[600].withOpacity(0.5),
                              offset: Offset(1.1, 1.1),
                              blurRadius: 10.0,
                            ),
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 80, vertical: 10),
                          child: Text(
                            'valider',
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              );
            }
            return Center(
                child:
                    CircularProgressIndicator(backgroundColor: Colors.orange));
          },
        ),
      ],
    );
  }
}

//** Place Tile */
class PlaceTile extends StatelessWidget {
  final Place place;
  const PlaceTile({
    Key key,
    @required this.place,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0),
      child: InkWell(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 150,
              width: 150,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  "${place.img.elementAt(Random().nextInt(place.img.length))}",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(width: 15),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      "${place.name}",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: Text(
                      "${place.libele}",
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      maxLines: 2,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Row(
                    children: [
                      Icon(Icons.api_rounded),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "${place.etat}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.green,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: FlatButton(
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 15),
                        onPressed: () {},
                        child: Text("Reservation")),
                  )
                ],
              ),
            ), /* 
          Container(
            height: 80,
            width: MediaQuery.of(context).size.width - 130,
            child: ListView(
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${place["name"]}",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                    ),
                    maxLines: 2,
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(height: 3),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.location_on,
                      size: 13,
                      color: Colors.blueGrey[300],
                    ),
                    SizedBox(width: 3),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "${place["location"]}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                          color: Colors.blueGrey[300],
                        ),
                        maxLines: 1,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${place["price"]}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                    maxLines: 1,
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ),
        */
          ],
        ),
        onTap: () {
          /* Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return Details(place: place);
              },
            ),
          ); */
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return PlaceInfoScreen(place: place);
              },
            ),
          );
        },
      ),
    );
  }
}

class MultiDeleteRow extends StatelessWidget {
  const MultiDeleteRow({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(
      buildWhen: (prev, next) => next is FavoriteCategoriesLoaded,
      builder: (context, state) {
        return Container(
          height: 35,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.max,
            children: [
              if (state is FavoriteCategoriesLoaded &&
                  state.favoriteToRemoveCount > 0)
                InkWell(
                  onTap: () {
                    context.read<CategoryBloc>().add(UpdateFavoriteCategories(
                        state.favoritecategoriesForChoice
                            .where((item) => item.isFavorite)
                            .map((e) => e.category)
                            .toList()));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.red[50],
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    ),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Text("supprimer(${state.favoriteToRemoveCount})"),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
            ],
          ),
        );
      },
    );
  }
}

// TODO: Revoir Details
class CategoryItem extends StatefulWidget {
  final Category category;
  final VoidCallback onLongPress;
  final bool selected;

  const CategoryItem({Key key, this.category, this.onLongPress, this.selected})
      : super(key: key);

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  //bool longPressed = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: InkWell(
        onLongPress: () {
          widget.onLongPress();
          /*  setState(() {
           // longPressed = !longPressed;
            widget.onLongPress();
          }); */
        },
        onTap: () {
          //TODO: mettre le push de l'écran en commentaire
          /*  Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return Details(place: places2.first);
              },
            ),
          ); */
          context
              .read<PlaceBloc>()
              .add(LoadPlacesForCategory(widget.category.key));
        },
        child: Container(
          height: 140,
          width: 90,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            border: Border.all(
              width: 1,
              color: widget.selected ? Colors.red : Colors.white,
            ),
          ),
          child: Center(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: SvgPicture.asset(
                widget.category.img,
                color: Colors.black,
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
                height: 65,
                width: 65,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CategoryItemToFavorite extends StatefulWidget {
  final CategoryForChooseFavorite categoryForChoose;
  final void Function(CategoryForChooseFavorite item) onTap;
  const CategoryItemToFavorite({Key key, this.categoryForChoose, this.onTap})
      : super(key: key);
  @override
  _CategoryItemToFavoriteState createState() => _CategoryItemToFavoriteState();
}

class _CategoryItemToFavoriteState extends State<CategoryItemToFavorite> {
  Category category;
  @override
  void initState() {
    this.category = widget.categoryForChoose.category;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => widget.onTap(widget.categoryForChoose
          .copyWith(isFavorite: !widget.categoryForChoose.isFavorite)),
      child: Container(
        width: 80,
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: Material(
          elevation: 4,
          shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(Radius.circular(8.0))),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
            child: Stack(
              children: [
                Container(
                  color: widget.categoryForChoose.isFavorite
                      ? Color(0xffFF9E4D)
                      : Colors.grey[50],
                ),
                Positioned(
                  bottom: -15,
                  right: -12,
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      color: widget.categoryForChoose.isFavorite
                          ? Colors.white
                          : Colors.orange,
                    ),
                  ),
                ),
                Positioned(
                  bottom: -18,
                  right: -15,
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16.0)),
                      color: widget.categoryForChoose.isFavorite
                          ? Color(0xffFEB13F)
                          : Colors.grey[200],
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: SvgPicture.asset(
                      category.img,
                      color: widget.categoryForChoose.isFavorite
                          ? Colors.white
                          : Colors.black,
                      allowDrawingOutsideViewBox: false,
                      alignment: Alignment.center,
                      height: 45,
                      width: 45,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
