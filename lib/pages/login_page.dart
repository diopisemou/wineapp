import 'package:flutter/material.dart';
import 'package:wineapp/constants/locale_fr.dart';
import 'package:wineapp/helpers/flutter_login/flutter_login.dart';
import 'package:wineapp/pages/navigation_home_screen.dart';
import 'package:wineapp/services/authentication.dart';

const users = {
  'admin@wine.com': 'testappwine',
  'dribbble@gmail.com' : '12345',
  'hunter@gmail.com': 'hunter',
};

class LoginPage extends StatelessWidget {
  LoginPage({this.auth, this.loginCallback, this.signUpCallback, this.forgotPasswordCallback});
  Duration get loginTime => Duration(milliseconds: 2250);
  
  final BaseAuth auth;
  final VoidCallback loginCallback;
  final VoidCallback signUpCallback;
  final VoidCallback forgotPasswordCallback;

  Future<String> _loginUser(LoginData data) {
    print('Name: ${data.username}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.username)) {
        return 'Username not exists';
      }
      if (users[data.username] != data.password) {
        return 'Password does not match';
      }

      // Navigator.of(context).push(
      //                   MaterialPageRoute(
      //                     builder: (BuildContext context) {
      //                       return NavigationHomeScreen();
      //                     },
      //                   ),
      //                 );

      return null;
    });

    
  }

  Future<String> _signUpUser(LoginData data) {
    print('Name: ${data.username}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.username)) {
        return 'Username not exists';
      }
      if (users[data.username] != data.password) {
        return 'Password does not match';
      }
      return null;
    });
  }

  Future<String> _recoverPassword(String name) {
    print('Name: $name');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(name)) {
        return 'Username not exists';
      }
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {

    final inputBorder = BorderRadius.vertical(
      bottom: Radius.circular(10.0),
      top: Radius.circular(20.0),
    );
    final roundedInputBorder = BorderRadius.circular(100);

    return FlutterLogin(
      title: 'Bienvenue',
      description : "Trouvez l'endroit idéal grâce à Wine App ! \n À partir de votre smartphone ou de votre PC",
      logo: 'assets/images/wineapp.png',
      onLogin: _loginUser,
      onSignup: _signUpUser,
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => NavigationHomeScreen(), //DashboardPage(auth: this.auth, logoutCallback: this.loginCallback)
        ));
      },
      onRecoverPassword: _recoverPassword,
      theme: LoginTheme(
        cardTheme: CardTheme(
          color: Colors.transparent,
          elevation: 0,
          margin: EdgeInsets.only(top: 15),
          shape: null,
        ),
        primaryColor: Colors.deepOrange[200],
        accentColor: Colors.white,
        errorColor: Colors.deepOrange,
        titleStyle: TextStyle(
          color: Colors.white,
          fontFamily: 'Quicksand',
          letterSpacing: 4,
        ),
        bodyStyle: TextStyle(
          fontStyle: FontStyle.italic,
          decoration: TextDecoration.underline,
          color: Colors.white,
        ),
        textFieldStyle: TextStyle(
          color: Colors.white,
          decorationColor: Colors.white,
          backgroundColor: Colors.transparent,
          //shadows: [Shadow(color: Colors.white, blurRadius: 2)],
        ),
        buttonStyle: TextStyle(
          fontWeight: FontWeight.w800,
          color: Colors.white,
        ),
        inputTheme: InputDecorationTheme(
          filled: true,
          fillColor: Colors.white.withOpacity(.1),
          focusColor: Colors.white,
          contentPadding: EdgeInsets.zero,
          errorStyle: TextStyle(
            backgroundColor: Colors.transparent,
            color: Colors.white,
            fontSize: 14,
          ),
          labelStyle: TextStyle(fontSize: 18, color: Colors.white,),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: .7),
            borderRadius: roundedInputBorder,
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 7),
            borderRadius: inputBorder,
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 8),
            borderRadius: inputBorder,
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 5),
            borderRadius: inputBorder,
          ),
        ),
        buttonTheme: LoginButtonTheme(
          splashColor: Colors.orange[50],
          backgroundColor: Colors.orangeAccent[200],
          highlightColor: Colors.lightGreen,
          backgroundImage: Image.asset('assets/images/bgloginbutton.png'),
          elevation: 9.0,
          highlightElevation: 6.0,
          // shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          // shape: CircleBorder(side: BorderSide(color: Colors.green)),
          // shape: ContinuousRectangleBorder(borderRadius: BorderRadius.circular(55.0)),
        ),
      ),
      messages: LoginMessages(

        usernameHint: text_local['EMAIL'],
        passwordHint: text_local['PASSWORD'],
        confirmPasswordHint: 'Confirm',
        loginButton: text_local['CONNEXION'],
        signupButton: text_local['REGISTER'],
        forgotPasswordButton: 'Mot de passe oublie ?',
        recoverPasswordButton: 'HELP ME',
        goBackButton: 'GO BACK',
        confirmPasswordError: 'Not match!',
        recoverPasswordDescription:
            'Recuperation de votre mot de passe',
        recoverPasswordSuccess: 'Recuperation avec succees',
      ),
    );
  }
}