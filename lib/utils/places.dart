import 'package:flutter/material.dart';
import 'package:wineapp/models/place.dart';

/* List places2 = [
  Palce(
    'place1',
    "color1": Color.fromARGB(100, 0, 0, 0).toString(),
    "color2": Color.fromARGB(100, 0, 0, 0).toString(),
    "name": "Hotel Dolah Amet & Suites",
    "img": "assets/images/1.jpeg",
    "price": r"$100/night",
    "etat": "ouvert",
    "libele": "Restaurant Bar",
    "location": "London, England",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\n\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  ),
  {
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "name": "Beach Mauris Blandit",
    "img": "assets/images/2.jpeg",
    "price": r"$100/night",
    "etat": "ouvert",
    "libele": "Restaurant Bar",
    "location": "Lisbon, Portugal",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
  {
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "name": "Ipsum Restaurant",
    "img": "assets/images/3.jpeg",
    "price": r"$100/night",
    "etat": "ouvert",
    "libele": "Restaurant Bar",
    "location": "Paris, France",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\n\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
  {
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "name": "Curabitur Beach",
    "img": "assets/images/4.jpeg",
    "price": r"$100/night",
    "etat": "ouvert",
    "libele": "Restaurant Bar",
    "location": "Rome, Italy",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
  {
    "color1": Color.fromARGB(100, 0, 0, 0),
    "color2": Color.fromARGB(100, 0, 0, 0),
    "name": "Tincidunt Pool",
    "img": "assets/images/5.jpeg",
    "price": r"$100/night",
    "etat": "ouvert",
    "libele": "Restaurant Bar",
    "location": "Madrid, Spain",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
];
 */



  List<Place> places2 = <Place>[
    Place.optional(
      key: 'place1',
      color1: "couleur1",
      color2: "couleur2",
      name: "Hotel Dolah Amet & Suites",
      img: ["assets/images/1.jpeg"],
      price: r"$100/night",
      etat: "ouvert",
      libele: "Restaurant Bar",
      location: "London, England",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat1",
    ),
    Place.optional(
      key: 'place2',
      color1: "couleur1",
      color2: "couleur2",
      name: "Beach Mauris Blandit",
      img: ["assets/images/2.jpeg"],
      price: r"$100/night",
      location: "Lisbon, Portugal",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: 'cat2',
    ),
    Place.optional(
      key: 'place3',
      color1: "couleur1",
      color2: "couleur2",
      name: "Ipsum Restaurant",
      img: ["assets/images/3.jpeg"],
      price: r"$100/night",
      location: "Paris, France",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat3",
    ),
    Place.optional(
        key: 'place4',
        color1: Color.fromARGB(100, 0, 0, 0).toString(),
        color2: Color.fromARGB(100, 0, 0, 0).toString(),
        name: "Curabitur Beach",
        img: ["assets/images/4.jpeg"],
        price: r"$100/night",
        location: "Rome, Italy",
        details: "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
        keyCategory: "cat3"),
    Place.optional(
      key: "place5",
      color1: Color.fromARGB(100, 0, 0, 0).toString(),
      color2: Color.fromARGB(100, 0, 0, 0).toString(),
      name: "Tincidunt Pool",
      img: ["assets/images/5.jpeg"],
      price: r"$100/night",
      location: "Madrid, Spain",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat3",
    ),
  ];

