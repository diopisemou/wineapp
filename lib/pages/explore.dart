
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/category/category_bloc.dart';
import 'package:wineapp/pages/popular_categories_list_view.dart';
import 'package:wineapp/utils/app_theme.dart';
import 'package:wineapp/widgets/carousel_place.dart';

class Explore extends StatefulWidget {
  const Explore({Key key, this.callBack}) : super(key: key);

  final Function callBack;
  @override
  _ExploreState createState() => _ExploreState();
}

class _ExploreState extends State<Explore> with TickerProviderStateMixin {
  final TextEditingController _searchControl = new TextEditingController();
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  void moveTo() {
    // Navigator.push<dynamic>(
    //   context,
    //   MaterialPageRoute<dynamic>(
    //     builder: (BuildContext context) => CourseInfoScreen(),
    //   ),
    // );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.nearlyWhite,
      child: Scaffold(
        // backgroundColor: Colors.transparent,
        backgroundColor: Colors.grey[300],
        body: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      /** Partie caroussel */
                      CarouselPlace(),
                      Expanded(child: getPopularCourseUI()),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //   Widget getPopularCourseUI() {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Text(
  //           'Popular Course',
  //           textAlign: TextAlign.left,
  //           style: TextStyle(
  //             fontWeight: FontWeight.w600,
  //             fontSize: 22,
  //             letterSpacing: 0.27,
  //             color: DesignCourseAppTheme.darkerText,
  //           ),
  //         ),
  //         Flexible(
  //           child: PopularCategoriesListView(
  //             callBack: () {
  //               moveTo();
  //             },
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  Widget getPopularCourseUI() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Popular Course',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: AppTheme.darkerText,
            ),
          ),
          Expanded(child: BlocBuilder<CategoryBloc, CategoryState>(
              buildWhen: (prState, nextState) => nextState is CategoryLoaded,
              builder: (context, state) {
                if (state is CategoryLoaded)
                  return PopularCategoriesListView(
                    categories: state.categories,
                    callBack: (_) {
                      moveTo();
                    },
                  );
                return Container();
              },
            ),
          ),
          /* Flexible(
            child: BlocBuilder<CategoryBloc, CategoryState>(
              buildWhen: (prState, nextState) => nextState is CategoryLoaded,
              builder: (context, state) {
                if (state is CategoryLoaded)
                  return PopularCategoriesListView(
                    categories: state.categories,
                    callBack: (_) {
                      moveTo();
                    },
                  );
                return Container();
              },
            ),
          ), */
        ],
      ),
    );
  }
}
