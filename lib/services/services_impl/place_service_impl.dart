import 'package:flutter/material.dart';
import 'package:wineapp/models/place.dart';
import 'package:wineapp/services/place_service.dart';

class PlaceServiceImpl implements PlaceService {
  @override
  Future<List<Place>> findPlaces(String keyWord, {int page}) async {
    await Future.delayed(Duration(seconds: 1));
    return _places;
  }

  @override
  Future<List<Place>> places({int page}) async {
    await Future.delayed(Duration(seconds: 1));
    return _places;
  }

  @override
  Future<List<Place>> findPlacesByCategory(String keyCategory) async {
    await Future.delayed(Duration(milliseconds: 500));
    return _places
        .where((element) => element.keyCategory == keyCategory)
        .toList();
  }

  @override
  Future<List<Place>> findPlacesByCategoryPage(String keyCategory, {int page}) {
    throw UnimplementedError();
  }

  @override
  Future<List<Place>> getSuggestions(String pattern) async {
    await Future.delayed(Duration(microseconds: 500));
    return _places
        .where((element) =>
            element.name.toLowerCase().contains(pattern.toLowerCase()))
        .toList();
  }

  List<Place> _places = <Place>[
    Place.optional(
      key: 'place1',
      color1: "couleur1",
      color2: "couleur2",
      name: "Hotel Dolah Amet & Suites",
      img: ["assets/images/1.jpeg"],
      price: r"$100/night",
      etat: "ouvert",
      libele: "Restaurant Bar",
      location: "London, England",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat1",
      webSite: 'www.monsit.com',
      telephone: '52365896',
      hasTerasse: true,
      hasParking: true,
      hasResevation: true,
      hasFumeur: true,
      hasWifi: true,
      rate: 2.2,
      heureOuverture: '8h00',
      heureFermeture: '00h00',
    ),
    Place.optional(
      key: 'place2',
      color1: "couleur1",
      color2: "couleur2",
      name: "Beach Mauris Blandit",
      img: ["assets/images/2.jpeg"],
      price: r"$100/night",
      libele: 'Restaurant Bar',
      etat: 'ouvert',
      location: "Lisbon, Portugal",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: 'cat2',
      webSite: 'www.monsit.com',
      telephone: '52365896',
      hasTerasse: true,
      hasParking: false,
      hasResevation: true,
      hasFumeur: false,
      hasWifi: true,
      rate: 3.2,
      heureOuverture: '8h00',
      heureFermeture: '00h00',
    ),
    Place.optional(
      key: 'place3',
      color1: "couleur1",
      color2: "couleur2",
      name: "Ipsum Restaurant",
      img: ["assets/images/3.jpeg"],
      price: r"$100/night",
      libele: 'Restaurant Bar',
      etat: 'ouvert',
      location: "Paris, France",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat3",
      webSite: 'www.monsit.com',
      telephone: '52715896',
      hasTerasse: false,
      hasParking: true,
      hasResevation: false,
      hasFumeur: false,
      hasWifi: true,
      rate: 2.2,
      heureOuverture: '8h00',
      heureFermeture: '00h00',
    ),
    Place.optional(
      key: 'place4',
      color1: Color.fromARGB(100, 0, 0, 0).toString(),
      color2: Color.fromARGB(100, 0, 0, 0).toString(),
      name: "Curabitur Beach",
      img: ["assets/images/4.jpeg"],
      price: r"$100/night",
      libele: 'Restaurant Bar',
      etat: 'ouvert',
      location: "Rome, Italy",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat3",
      webSite: 'www.monsit.com',
      telephone: '52005896',
      hasTerasse: true,
      hasParking: true,
      hasResevation: true,
      hasFumeur: true,
      hasWifi: true,
      rate: 2.2,
      heureOuverture: '8h00',
      heureFermeture: '00h00',
    ),
    Place.optional(
      key: "place5",
      color1: Color.fromARGB(100, 0, 0, 0).toString(),
      color2: Color.fromARGB(100, 0, 0, 0).toString(),
      name: "Tincidunt Pool",
      img: ["assets/images/5.jpeg"],
      price: r"$100/night",
      libele: 'Restaurant Bar',
      etat: 'ouvert',
      location: "Madrid, Spain",
      details: "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      keyCategory: "cat3",
      webSite: 'www.myplace.com',
      telephone: '42365896',
      hasTerasse: true,
      hasParking: true,
      hasResevation: true,
      hasFumeur: true,
      hasWifi: true,
      rate: 2.2,
      heureOuverture: '8h00',
      heureFermeture: '00h00',
    ),
  ];
}
