import 'package:wineapp/models/evenement.dart';
import 'package:wineapp/services/evenement_service.dart';

class EvenementServiceImpl implements EvenementService {
  @override
  Future<List<Evenement>> evenements({int page}) async {
    await Future.delayed(Duration(seconds: 1));
    return _evenements;
  }

  final List<Evenement> _evenements = [
    Evenement.optonal(
      key: 'ev1',
      libele: 'evenement 1',
      urlImage: 'https://avatars0.githubusercontent.com/u/12619420?s=460&v=4',
    ),
    Evenement.optonal(
      key: 'ev2',
      libele: 'evenement 2',
      urlImage: 'https://avatars0.githubusercontent.com/u/12619420?s=460&v=4',
    ),
    Evenement.optonal(
      key: 'ev2',
      libele: 'evenement 3',
      urlImage: 'https://avatars0.githubusercontent.com/u/12619420?s=460&v=4',
    ),
    Evenement.optonal(
      key: 'ev2',
      libele: 'evenement 4',
      urlImage: 'https://avatars0.githubusercontent.com/u/12619420?s=460&v=4',
    ),
    Evenement.optonal(
      key: 'ev2',
      libele: 'evenement 5',
      urlImage: 'https://avatars0.githubusercontent.com/u/12619420?s=460&v=4',
    ),
  ];
}
