import 'package:rxdart/rxdart.dart';
import 'package:wineapp/models/notification.dart';

import '../notification_sevice.dart';

class NotificationServiceImpl implements NotificationService {
  BehaviorSubject<List<MyNotification>> _controler =
      BehaviorSubject<List<MyNotification>>();

  NotificationServiceImpl() {
    _controler.add(_notifications);
  }

  @override
  Future<void> deleteNotification(MyNotification notification) async {
    await Future.delayed(Duration(seconds: 1));
    _notifications = _notifications
        .where((element) => element.id != notification.id)
        .toList();
    _controler.add(_notifications);
  }

  @override
  Stream<List<MyNotification>> notifications() {
    return _controler.stream;
  }

  @override
  Future<void> visit(MyNotification notification) {
    _notifications = _notifications
        .map((elmt) => elmt.id == notification.id ? notification : elmt)
        .toList();
    _controler.add(_notifications);
  }

  List<MyNotification> _notifications = List<MyNotification>.generate(
    100,
    (index) => MyNotification(
      id: '$index',
      date: DateTime.now().subtract(Duration(hours: index)),
      message: 'ceci est la notification numero $index',
      title: 'titre $index',
      visited: false,
    ),
  );
}
