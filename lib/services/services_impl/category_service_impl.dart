import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wineapp/models/category.dart';
import 'package:wineapp/services/category_service.dart';

class CategoryServiceImpl implements CategoryService {
  BehaviorSubject<List<Category>> _controller =
      BehaviorSubject<List<Category>>();
  BehaviorSubject<List<Category>> _controllerFavoriteCategories =
      BehaviorSubject<List<Category>>();

  @override
  Stream<List<Category>> categories() {
    return _controller.stream;
  }

  @override
  Stream<List<Category>> favoriteCategories() {
    return _controllerFavoriteCategories.stream;
  }

  @override
  Future<void> updateFavoriteCategories(List<Category> categories) async {
    _favoriteCategories = categories;
    _controllerFavoriteCategories.add(_favoriteCategories);
  }

  List<Category> _favoriteCategories = [];

  List<Category> _categories = [];

  CategoryServiceImpl() {
    _favoriteCategories = [
      Category(
        'cat1',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Restaurant",
        "assets/icon/fork.svg",
        r"$100/night",
        "London, England",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\n\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      ),
      Category(
        'cat2',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Bar",
        "assets/icon/beer.svg",
        r"$100/night",
        "Lisbon, Portugal",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      ),
    ];
    _categories = [
      ..._favoriteCategories,
      Category(
        'cat3',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Bank",
        "assets/icon/price.svg",
        r"$100/night",
        "Paris, France",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\n\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      ),
      Category(
        'cat4',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Sante",
        "assets/icon/medicine.svg",
        r"$100/night",
        "Rome, Italy",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      ),
      Category(
        'cat5',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Boite de nuit",
        "assets/icon/disco-ball.svg",
        r"$100/night",
        "Madrid, Spain",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      ),
      Category(
        'cat6',
        Colors.black.toString(),
        Colors.orange[600].toString(),
        "Alarm",
        "assets/icon/alarm.svg",
        r"$100/night",
        "Madrid, Spain",
        "Pellentesque in ipsum id orci porta dapibus. "
            "Nulla porttitor accumsan tincidunt. Donec rutrum "
            "congue leo eget malesuada. "
            "\nPraesent sapien massa, convallis a pellentesque "
            "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
            "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
            "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
            "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
            "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
      )
    ];

    _controller.add(_categories);
    _controllerFavoriteCategories.add(_favoriteCategories);
  }

  List categories2 = [
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Restaurant",
      "img": "assets/icon/fork.svg",
      "price": r"$100/night",
      "location": "London, England",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    },
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Bar",
      "img": "assets/icon/beer.svg",
      "price": r"$100/night",
      "location": "Lisbon, Portugal",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    },
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Bank",
      "img": "assets/icon/price.svg",
      "price": r"$100/night",
      "location": "Paris, France",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\n\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    },
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Sante",
      "img": "assets/icon/medicine.svg",
      "price": r"$100/night",
      "location": "Rome, Italy",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    },
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Boite de nuit",
      "img": "assets/icon/disco-ball.svg",
      "price": r"$100/night",
      "location": "Madrid, Spain",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    },
    {
      "color1": Colors.black,
      "color2": Colors.orange[600],
      "name": "Alarm",
      "img": "assets/icon/alarm.svg",
      "price": r"$100/night",
      "location": "Madrid, Spain",
      "details": "Pellentesque in ipsum id orci porta dapibus. "
          "Nulla porttitor accumsan tincidunt. Donec rutrum "
          "congue leo eget malesuada. "
          "\nPraesent sapien massa, convallis a pellentesque "
          "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
          "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
          "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
          "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
          "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
    }
  ];
}
