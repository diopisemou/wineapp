import 'package:wineapp/models/place.dart';

abstract class PlaceService {
  Future<List<Place>> findPlaces(String keyWord, {int page});
  Future<List<Place>> places({int page});
  Future<List<Place>> findPlacesByCategoryPage(String keyCategory, {int page});
  Future<List<Place>> findPlacesByCategory(String keyCategory);
  // get suggestion by pattern
  Future<List<Place>> getSuggestions(String pattern);
}
