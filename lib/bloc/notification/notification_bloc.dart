import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:wineapp/models/notification.dart';
import 'package:wineapp/services/notification_sevice.dart';

part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationService _notificationService;
  StreamSubscription _notificationsSubscription;

  NotificationBloc({@required NotificationService notificationService})
      : assert(notificationService != null),
        _notificationService = notificationService,
        super(NotificationInitial());

  @override
  Stream<NotificationState> mapEventToState(
    NotificationEvent event,
  ) async* {
    if (event is LoadNotification) {
      yield* _mapLoadNotificationToState();
    } else if (event is NotificationsUpdated) {
      yield* _mapNotificationsUpdatedToState(event);
    } else if (event is DeleteNotification) {
      yield* _mapDeleteNotificationToState(event);
    } else if (event is VisitNotification) {
      yield* _mapVisitNotificationToState(event);
    }
  }

  Stream<NotificationState> _mapLoadNotificationToState() async* {
    _notificationsSubscription?.cancel();
    _notificationsSubscription = _notificationService.notifications().listen(
          (notifs) => add(NotificationsUpdated(notifs)),
        );
  }

  Stream<NotificationState> _mapDeleteNotificationToState(
      DeleteNotification event) async* {
    await _notificationService.deleteNotification(event.notification);
  }

  Stream<NotificationState> _mapNotificationsUpdatedToState(
      NotificationsUpdated event) async* {
    yield NotificationLoaded(event.notifications);
  }

  Stream<NotificationState> _mapVisitNotificationToState(
      VisitNotification event) async* {
    await _notificationService.visit(event.notification);
  }
}
