import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wineapp/models/category.dart';
import 'package:wineapp/utils/app_theme.dart';
import 'package:wineapp/utils/categories.dart';
import 'package:wineapp/utils/utils.dart';
import 'package:wineapp/utils/extension.dart';

class PopularCategoriesListView extends StatefulWidget {
  final List<Category> categories;
  const PopularCategoriesListView({
    Key key,
    this.callBack,
    @required this.categories,
  }) : super(key: key);

  final Function(String keyCategory) callBack;
  @override
  _PopularCategoriesListViewState createState() =>
      _PopularCategoriesListViewState();
}

class _PopularCategoriesListViewState extends State<PopularCategoriesListView>
    with TickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: FutureBuilder<bool>(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox();
          } else {
            return GridView(
              padding: const EdgeInsets.all(8),
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: [
                ...(widget.categories.mapIndexed<Material>((e, index) {
                  final int count = categories.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                    CurvedAnimation(
                      parent: animationController,
                      curve: Interval((1 / count) * index, 1.0,
                          curve: Curves.fastOutSlowIn),
                    ),
                  );
                  animationController.forward();
                  return Material(
                    borderRadius: BorderRadius.all(Radius.circular(16.0)),
                    color: Colors.white,
                    shadowColor: Colors.white,
                    type: MaterialType.card,
                    elevation: 8,
                    child: CategoryView(
                      callback: () {
                        widget.callBack(e.key);
                      },
                      category: categories[index],
                      animation: animation,
                      animationController: animationController,
                    ),
                  );
                }).toList()),
                SizedBox(
                  height: 100,
                )
              ],
              /*  List<Widget>.generate(
                categories.length,
                (int index) {
                }, */

              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 22.0,
                crossAxisSpacing: 22.0,
                childAspectRatio: 1,
              ),
            );
          }
        },
      ),
    );
  }
}

class CategoryView extends StatelessWidget {
  const CategoryView(
      {Key key,
      this.category,
      this.animationController,
      this.animation,
      this.callback})
      : super(key: key);

  final VoidCallback callback;
  final Category category;
  final AnimationController animationController;
  final Animation<dynamic> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: () {
                //xx
                callback();
              },
              child: SizedBox(
                height: 170,
                child: Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    Container(
                      height: 170,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 170,
                              decoration: BoxDecoration(
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color:
                                        AppTheme.nearlyWhite.withOpacity(0.5),
                                    offset: const Offset(4.0, 0.0),
                                    blurRadius: 8,
                                    spreadRadius: 2,
                                  ),
                                ],
                                color: HexColor('#F8FAFB'),
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(16.0)),
                                // border: new Border.all(
                                //     color: AppTheme.notWhite),
                              ),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 8, right: 16, left: 16),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(16.0)),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                                color: AppTheme.white
                                                    .withOpacity(0.2),
                                                offset: const Offset(0.0, 0.0),
                                                blurRadius: 6.0),
                                          ],
                                        ),
                                        child: ClipRRect(
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(16.0)),
                                          child: AspectRatio(
                                              aspectRatio: 1.2,
                                              child: SvgPicture.asset(
                                                category.img,
                                                color: Colors.black,
                                                allowDrawingOutsideViewBox:
                                                    false,
                                                alignment: Alignment.center,
                                                height: 45,
                                                width: 45,
                                              )),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 12, left: 16, right: 16),
                                            child: Text(
                                              category.name,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                letterSpacing: 0.20,
                                                color: AppTheme.darkerText,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
