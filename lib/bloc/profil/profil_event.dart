part of 'profil_bloc.dart';

abstract class ProfilEvent {
  ProfilEvent();
}

class DisplayProfil extends ProfilEvent {
  @override
  String toString() => 'DisplayProfil';
}

class UpdateProfil extends ProfilEvent {
  @override
  String toString() => 'Mise à jour de du profil';
}

class UpdateFullName extends ProfilEvent {
  final String fullName;
  UpdateFullName({@required this.fullName});

  @override
  String toString() => 'UpdateFullName(fullName: $fullName)';
}

class UpdateEmail extends ProfilEvent {
  final String email;
  UpdateEmail({@required this.email});

  @override
  String toString() => 'UpdateEmail(email: $email)';
}

class DisplayChangePassWord extends ProfilEvent {
  @override
  String toString() => 'DisplayChangePasseWord';
}
class PasswordInputChanged extends ProfilEvent {
  final String password;

  PasswordInputChanged(this.password);

}


class UpdatePassword extends ProfilEvent {
  final String oldPassword;
  final String newPassword;

  UpdatePassword({this.oldPassword, this.newPassword});

  @override
  String toString() =>
      'UpdatePassword(oldPassword: $oldPassword, newPassword: $newPassword)';
}
