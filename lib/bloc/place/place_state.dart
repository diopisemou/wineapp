part of 'place_bloc.dart';

abstract class PlaceState extends Equatable {
  const PlaceState();

  @override
  List<Object> get props => [];
}

class PlaceInitial extends PlaceState {
  @override
  String toString() => "PlaceInitial";
}

class PlaceLoaded extends PlaceState {
  final List<Place> places;

  PlaceLoaded(this.places);
  List<Object> get props => [places];
  @override
  String toString() => "PlaceLoaded";
}

class PlaceForCategoryLoaded extends PlaceState {
  final List<Place> places;

  PlaceForCategoryLoaded(this.places);
  List<Object> get props => [places];
  @override
  String toString() => "PlaceForCategoryLoaded";
}
