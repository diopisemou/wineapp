import 'package:flutter/material.dart';
import 'package:wineapp/widgets/custom_button_edit_profil.dart';

class FormFullName extends StatefulWidget {
  final Function(String fullName) onSave;
  final String currentValue;
  FormFullName({Key key, this.onSave, @required this.currentValue})
      : super(key: key);

  @override
  _FormFullNameState createState() => _FormFullNameState();
}

class _FormFullNameState extends State<FormFullName> {
  final _keyFormFullName = GlobalKey<FormState>();
  String _fullName;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _keyFormFullName,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          Text(
            'Saisir votre nom complet',
            style: TextStyle(fontSize: 17),
          ),
          TextFormField(
            initialValue: widget.currentValue,
            onSaved: (value) => _fullName = value,
            validator: (String value) {
              return value.contains('@') || value.isEmpty
                  ? 'Nom invalide'
                  : null;
            },
          ),
          CustomButtomEditProfil(
            formKey: _keyFormFullName,
            onSave: () => widget.onSave(_fullName),
          ),
         // SizedBox(height: MediaQuery.of(context).viewInsets.bottom + 10),
        ],
      ),
    );
  }
}
