part of 'evenement_bloc.dart';

abstract class EvenementEvent extends Equatable {
  const EvenementEvent();

  @override
  List<Object> get props => [];
}

class LoadEvenements extends EvenementEvent {
  @override
  String toString() => "LoadEvenements";
}
