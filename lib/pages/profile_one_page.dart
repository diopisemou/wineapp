import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/authentifiation/authentification_bloc.dart';
import 'package:wineapp/bloc/notification/notification_bloc.dart';
import 'package:wineapp/bloc/profil/profil_bloc.dart';

import 'package:wineapp/components/common_divider.dart';
import 'package:wineapp/components/common_scaffold.dart';
import 'package:wineapp/components/profile_tile.dart';
import 'package:wineapp/pages/compte_page.dart';
import 'package:wineapp/pages/notification_page.dart';
import 'package:wineapp/services/authentication.dart';
import 'package:wineapp/services/notification_sevice.dart';
import 'package:wineapp/utils/app_theme.dart';

class ProfileOnePage extends StatefulWidget {
  @override
  _ProfileOnePageState createState() => _ProfileOnePageState();
}

class _ProfileOnePageState extends State<ProfileOnePage>
    with TickerProviderStateMixin {
  final double infoHeight = 364.0;
  AnimationController animationController;
  Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000));
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();
    super.initState();
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  var deviceSize;

  Widget bodyData() {
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        24.0;
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
              pinned: true,
              floating: true,
              snap: true,
              bottom: PreferredSize(
                child: Container(),
                preferredSize: Size(0, 40),
              ),
              expandedHeight: 300.0,
              //stretch: false,

              flexibleSpace: Stack(
                overflow: Overflow.visible,
                children: [
                  Container(
                    height: deviceSize.height * 0.36,
                    width: double.infinity,
                    child: ClipRect(
                      child: Image.asset(
                        'assets/images/profilebg2.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: ScaleTransition(
                      scale: CurvedAnimation(
                          parent: animationController,
                          curve: Curves.fastOutSlowIn),
                      child: profileColumn(),
                    ),
                  ),
                  Positioned(
                    bottom: -80,
                    left: 0,
                    right: 0,
                    child: Center(
                        child: accountColumn(
                            context, deviceSize, animationController)),
                  ),
                ],
              )
              /* flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              titlePadding: EdgeInsets.all(0),
              title: ScaleTransition(
                scale: CurvedAnimation(
                    parent: animationController, curve: Curves.fastOutSlowIn),
                child: profileColumn(),
              ),
              background: AspectRatio(
                aspectRatio: 1.2,
                child: Image.asset(
                  'assets/images/profilebg2.png',
                  fit: BoxFit.cover,
                ),
              ),
            ), */
              ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                    height: 700.0,
                    child: SingleChildScrollView(
                        child: Column(
                      children: [
                        /*  accountColumn(deviceSize, animationController),
                        Divider(
                          color: Colors.grey.shade300,
                          height: 5.0,
                          thickness: 5,
                        ), */
                        SizedBox(height: 30),
                        CommonDivider(),
                        descColumn(),
                        CommonDivider(),
                        likedColumn(deviceSize),
                        CommonDivider(),
                        notifColumn(context, deviceSize),
                        CommonDivider(),
                        contactColumn(deviceSize),
                        SizedBox(
                          height: MediaQuery.of(context).padding.bottom,
                        ),
                      ],
                    )))
              ],
            ),
          ),
        ],
      ),
    );
    // SingleChildScrollView(
    //   child: Column(
    //     children: <Widget>[
    //       profileColumn(),
    //       accountColumn(deviceSize, animationController),
    //       Divider(
    //         color: Colors.grey.shade300,
    //         height: 5.0,
    //         thickness: 5,
    //       ),
    //       descColumn(),
    //       CommonDivider(),
    //       likedColumn(deviceSize),
    //       CommonDivider(),
    //       notifColumn(deviceSize),
    //       CommonDivider(),
    //       contactColumn(deviceSize),
    //     ],
    //   ),
    // );
  }

  Widget _scaffold() => CommonScaffold(
        appTitle: "",
        bodyData: bodyData(),
        showFAB: false,
        showDrawer: false,
        floatingIcon: null,
        showBottomNav: false,
      );

  @override
  Widget build(BuildContext context) {
    deviceSize = MediaQuery.of(context).size;
    return _scaffold();
  }

  //Column1
  Widget profileColumn() => Container(
        width: deviceSize.width,
        decoration: BoxDecoration(
          border: new Border.all(
            color: Colors.white,
            width: 1.0,
          ),
        ),
        height: deviceSize.height * 0.36,
        child: FittedBox(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        /*  decoration: BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(50.0)),
                          border: new Border.all(
                            color: Colors.white,
                            width: 0.0,
                          ),
                        ), */
                        child: CircleAvatar(
                          backgroundImage: AssetImage(
                              "assets/images/fredamvane.png"), //NetworkImage( "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
                          foregroundColor: Colors.black,
                          radius: 16.0,
                        ),
                      )
                    ],
                  ),
                ),
                ProfileTile(
                  title: "Frederic Amvane",
                  subtitle: "Lorem Ipsum Dolor Set",
                  textColor: Colors.white,
                ),
              ],
            ),
          ),
        ),
      );

  //column3
  Widget descColumn() => Container(
        height: deviceSize.height * 0.08,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 12,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 6, right: 20),
              child: Center(
                child: Container(
                  width: 20,
                  height: 30,
                ),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10, right: 26),
                child: Center(
                  child: Text(
                    'Mon Addresse ',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                      letterSpacing: 0.27,
                      color: AppTheme.nearlyBlack,
                    ),
                  ),
                )),
            SizedBox(
              width: 110,
            ),
          ],
        ),
      );
}

Widget accountColumn(
        BuildContext context, Size deviceSize, animationController) =>
    Container(
      margin: const EdgeInsets.only(top: 0.0),
      child: ScaleTransition(
        alignment: Alignment.center,
        scale: CurvedAnimation(
            parent: animationController, curve: Curves.fastOutSlowIn),
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  bottomLeft: Radius.circular(16.0),
                  bottomRight: Radius.circular(16.0),
                  topRight: Radius.circular(16.0))),
          elevation: 10.0,
          child: Container(
            width: deviceSize.width * 0.95,
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, left: 20, right: 20),
                  child: Icon(
                    Icons.person,
                    color: AppTheme.nearlyBlue,
                    size: 30,
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      InkWell(
                        onTap: () {
                          /** Modification du compte */
                          Navigator.push<dynamic>(
                            context,
                            MaterialPageRoute<dynamic>(
                              builder: (_) => BlocProvider<ProfilBloc>(
                                create: (__) => ProfilBloc(
                                  baseAuth: context.read<BaseAuth>(),
                                )..add(DisplayProfil()),
                                child: Compte(),
                              ),
                            ),
                          );
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Compte ',
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                                letterSpacing: 0.27,
                                color: AppTheme.nearlyBlack,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10.0, right: 18),
                              child: Center(
                                child: Container(
                                  width: 20,
                                  height: 30,
                                  child: Center(
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.orange[600],
                                      size: 30,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Couleur du profile ',
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 22,
                              letterSpacing: 0.27,
                              color: AppTheme.nearlyBlack,
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, right: 20),
                            child: Center(
                              child: Container(
                                width: 20,
                                height: 30,
                                child: Center(
                                  child: Icon(
                                    Icons.radio_button_unchecked,
                                    color: Colors.orange[600],
                                    size: 30,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );

Widget likedColumn(Size deviceSize) => Container(
      height: deviceSize.height * 0.09,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: 12,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 6, right: 20),
            child: Center(
              child: Container(
                width: 20,
                height: 30,
                child: Center(
                  child: Icon(
                    Icons.favorite,
                    color: AppTheme.nearlyBlue,
                    size: 30,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 1, right: 26),
              child: Center(
                child: Text(
                  'Lieux Aimez ',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    letterSpacing: 0.27,
                    color: AppTheme.nearlyBlack,
                  ),
                ),
              )),
          SizedBox(
            width: 70,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, right: 18),
            child: Center(
              child: Container(
                width: 20,
                height: 30,
                child: Center(
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.orange[600],
                    size: 30,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

Widget notifColumn(BuildContext context, deviceSize) => InkWell(
      onTap: () {
        Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(
            builder: (BuildContext context) => BlocProvider<NotificationBloc>(
              create: (_) => NotificationBloc(
                notificationService: context.read<NotificationService>(),
              )..add(LoadNotification()),
              child: NotificationPage(),
            ),
          ),
        );
      },
      child: Container(
        height: deviceSize.height * 0.09,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 12,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 6, right: 20),
              child: Center(
                child: Container(
                  width: 20,
                  height: 30,
                  child: Center(
                    child: Icon(
                      Icons.notifications,
                      color: AppTheme.nearlyBlue,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 1, right: 26),
                child: Center(
                  child: Text(
                    'Notifications ',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      letterSpacing: 0.27,
                      color: AppTheme.nearlyBlack,
                    ),
                  ),
                )),
            SizedBox(
              width: 70,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, right: 18),
              child: Center(
                child: Container(
                  width: 20,
                  height: 30,
                  child: Center(
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.orange[600],
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

Widget contactColumn(Size deviceSize) => Container(
      height: deviceSize.height * 0.09,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: 12,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 6, right: 0),
            child: Center(
              child: Container(
                width: 0,
                height: 30,
              ),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 0, right: 30),
              child: Center(
                child: Text(
                  'Contacts  ',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                    letterSpacing: 0.27,
                    color: AppTheme.nearlyBlack,
                  ),
                ),
              )),
          SizedBox(
            width: 70,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, right: 18),
            child: Center(
              child: Container(
                width: 20,
                height: 30,
                child: Center(
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.orange[600],
                    size: 30,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

Widget followColumn(Size deviceSize) => Container(
      height: deviceSize.height * 0.13,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ProfileTile(
            title: "1.5K",
            subtitle: "Posts",
          ),
          ProfileTile(
            title: "2.5K",
            subtitle: "Followers",
          ),
          ProfileTile(
            title: "10K",
            subtitle: "Comments",
          ),
          ProfileTile(
            title: "1.2K",
            subtitle: "Following",
          )
        ],
      ),
    );
