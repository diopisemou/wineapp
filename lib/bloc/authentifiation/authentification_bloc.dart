import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wineapp/services/authentication.dart';

part 'authentification_event.dart';
part 'authentification_state.dart';

class AuthentificationBloc
    extends Bloc<AuthentificationEvent, AuthentificationState> {
  final BaseAuth _baseAuth;

  AuthentificationBloc({@required BaseAuth baseAuth})
      : assert(baseAuth != null),
        this._baseAuth = baseAuth,
        super(AuthentificationInitial());

  @override
  Stream<AuthentificationState> mapEventToState(
    AuthentificationEvent event,
  ) async* {
    if (event is LoggedIn) {
      yield* _mapLoggedInToState(event);
    }
  }

  Stream<AuthentificationState> _mapLoggedInToState(LoggedIn event) async* {
    final result = await _baseAuth.signIn(event.login, event.password);
  }
}
