import 'package:flutter/material.dart';
import 'package:wineapp/components/drawer_user_controller.dart';
import 'package:wineapp/pages/blanc_page.dart';
import 'package:wineapp/pages/home_drawer.dart';
import 'package:wineapp/pages/home_list.dart';
import 'package:wineapp/pages/tab_page_container.dart';
import 'package:wineapp/pages/profile_one_page.dart';
import 'package:wineapp/utils/app_theme.dart';
import 'explore.dart';

class NavigationHomeScreen extends StatefulWidget {
  @override
  _NavigationHomeScreenState createState() => _NavigationHomeScreenState();
}

class _NavigationHomeScreenState extends State<NavigationHomeScreen> {
  Widget screenView;
  DrawerIndex drawerIndex;

  @override
  void initState() {
    drawerIndex = DrawerIndex.HOME;
    screenView = TabPageContainer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.white,
      child: SafeArea(
        minimum: const EdgeInsets.only(left: 2, right: 2, top: 5),
        right: true,
        top: true,
        bottom: false,
        child: Scaffold(
          backgroundColor: AppTheme.white,
          body: DrawerUserController(
            animatedIconData: AnimatedIcons.arrow_menu,
            screenIndex: drawerIndex,
            drawerWidth: MediaQuery.of(context).size.width * 0.75,
            onDrawerCall: (DrawerIndex drawerIndexdata) {
              changeIndex(drawerIndexdata);
              //callback from drawer for replace screen as user need with passing DrawerIndex(Enum index)
            },
            screenView: screenView,
            //we replace screen view as we need on navigate starting screens like MyHomePage, HelpScreen, FeedbackScreen, etc...
          ),
        ),
      ),
    );
  }

  void changeIndex(DrawerIndex drawerIndexdata) {
    if (drawerIndex != drawerIndexdata) {
      drawerIndex = drawerIndexdata;
      if (drawerIndex == DrawerIndex.HOME) {
        setState(() {
          screenView = TabPageContainer();
        });
      } else if (drawerIndex == DrawerIndex.Help) {
        setState(() {
         // screenView = HomeList();
          screenView = BlancPage();
        });
      } else if (drawerIndex == DrawerIndex.FeedBack) {
        setState(() {
         // screenView = Explore();
         screenView = BlancPage();
        });
      } else if (drawerIndex == DrawerIndex.Invite) {
        setState(() {
         // screenView = ProfileOnePage();
          screenView = BlancPage();
        });
      } else {
        //do in your way......
      }
    }
  }
}
