import 'package:flutter/material.dart';
import 'package:wineapp/components/footerads_page.dart';
import 'package:wineapp/constants/constants.dart';
import 'package:wineapp/extensions/HexColor.dart';
import 'package:wineapp/pages/recovercode_page.dart';
import 'package:wineapp/services/authentication.dart';


class RecoverPasswordPage extends StatefulWidget {
  RecoverPasswordPage({this.auth, this.recoverCodeCallback});

  final BaseAuth auth;
  final void Function(dynamic verifCode) recoverCodeCallback;

  @override
  State<StatefulWidget> createState() => new _RecoverPasswordPageState();
}

class _RecoverPasswordPageState extends State<RecoverPasswordPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _phoneNumber;
  String _errorMessage;

  bool _isLoading;
  bool _smsOptionSelectd;
  bool _emailOptionSelectd;

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

// Start Password Recovery Workflow
  void selectSMSOption() async {
    setState(() {
      _errorMessage = "";
      _isLoading = false;
      _smsOptionSelectd = true;
      _emailOptionSelectd = false;
    });
    String userId = "";
      try {
        toggleFormMode(_smsOptionSelectd, _emailOptionSelectd);
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
  }

  // Start Password Recovery Workflow with email
  void selectEmailOption() async {
    setState(() {
      _errorMessage = "";
      _smsOptionSelectd = false;
      _emailOptionSelectd = true;
    });
    String userId = "";
      try {
        toggleFormMode(_smsOptionSelectd, _emailOptionSelectd);
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
  }



  // Start Password Recovery Workflow
  void validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (validateAndSave()) {
      String userId = "";
      String verifCode = APP_VERIFICATION_CODE;
        try {
          if (_smsOptionSelectd) {
            var smsResult = await widget.auth.sendSMSRecoveryWithCode(_phoneNumber, verifCode);
            
            if (smsResult['error_message'] == null && smsResult['error_code'] == null) {
              print('Success');
              print('Recovery with sms in: $smsResult');
              await _goNext(verifCode);
              //widget.recoverCodeCallback(verifCode);
            }
          } else if (_emailOptionSelectd) {
            await widget.auth.sendEmailRecoveryCode(_email);
            print('Recovery with email: $userId');
            await _goNext(verifCode);
            //widget.recoverCodeCallback(verifCode);
          }
          setState(() {
            _isLoading = false;
          });

          
        } catch (e) {
          print('Error: $e');
          setState(() {
            _isLoading = false;
            _errorMessage = e.message;
            _formKey.currentState.reset();
          });
        }
      }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _email = "";
    _phoneNumber = "";
    _errorMessage = "";
    _isLoading = false;
    _smsOptionSelectd = false;
    _emailOptionSelectd = false;


    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void toggleFormMode(bool _smsOptionSelectd, bool _emailOptionSelectd ) {
    resetForm();
    setState(() {
      _smsOptionSelectd = _smsOptionSelectd;
      _emailOptionSelectd = _emailOptionSelectd;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: new FooterAdsPage(adsText:'Advertising 7'),
      appBar: new AppBar(
          backgroundColor: Colors.white,
          leading: new FlatButton(
            child: Icon(
            // Based on passwordVisible state choose the icon
              Icons.arrow_back,
              size: 30.0,
              color: HexColor.fromHex(APP_COLOR_RED),
            ),
            onPressed: _goBack)
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              showPageHeader(),
              showRecoveryTextField(),
              showActionButton(),
              showErrorMessage()
            ],
          ),
        ));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

Widget showLogo() {
    return new Hero(
      tag: 'herologo',
      child: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 70.0,
          child: Image.asset('assets/images/KicksTrade.png'),
        ),
      ),
    );

  }


Widget showPasswordRecoveryLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 40.0, 30.0, 0.0),
      child: new Column(
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
           new Text('Password', style: new TextStyle(fontSize: 25.0, color: Colors.black, fontWeight: FontWeight.w600)),
           new Text('Recovery', style: new TextStyle(fontSize: 25.0, color: Colors.black, fontWeight: FontWeight.w600))
         ]
      ),
    );
  }



  Widget showPageHeader() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: new Column (
          children : <Widget>[
            new Row(
              children: <Widget>[
                showPasswordRecoveryLabel(),
                showLogo()
              ],
          )
          ]
        )
        );
  }


 


  Widget showActionButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
        child: new Column(
            children: <Widget>[
              showSMSButton(),
              showEmailButton(),
              showContinueButton()
            ],
          )
        );
  }



 Widget showSMSButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: SizedBox(
          height: 80.0,
          width: 200.0,
          child: new RaisedButton(
            elevation: 10.0,
            highlightElevation: 5.0,
            highlightColor: Colors.black,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            color: Colors.grey[300],
            child: new Row(
              children: <Widget>[
                Icon(Icons.perm_phone_msg, size: 30.0, color: Colors.grey, ),  // Based on passwordVisible state choose the icon 
                new Column( 
                  children : <Widget>[
                    new Text('', style: new TextStyle(fontSize: 20.0, color: Colors.transparent)),
                    new Text('  Via SMS ', style: new TextStyle(fontSize: 15.0, color: Colors.grey)),
                    new Text('   (###) ### - 1234', style: new TextStyle(fontSize: 13.0, color: Colors.grey))
                  ]
                )
              ],
            ),
            onPressed: selectSMSOption
          ),
        ));
  }



  Widget showEmailButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
        child: SizedBox(
          height: 80.0,
          width: 200.0,
          child: new RaisedButton(
            elevation: 10.0,
            highlightElevation: 5.0,
            highlightColor: Colors.black,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            color: Colors.grey[300],
            child: new Row(
              children: <Widget>[
                Icon(Icons.email, size: 30.0, color: Colors.grey, ),  // Based on passwordVisible state choose the icon 
                new Column( 
                  children : <Widget>[
                    new Text('', style: new TextStyle(fontSize: 20.0, color: Colors.transparent)),
                    new Text('  Via Email ', style: new TextStyle(fontSize: 15.0, color: Colors.grey)),
                    new Text('   ######@email.com', style: new TextStyle(fontSize: 13.0, color: Colors.grey))
                  ]
                )
              ],
            ),
            onPressed: selectEmailOption
          ),
        ));
  }


  Widget showContinueButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          width: 150.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            color: HexColor.fromHex(APP_COLOR_RED),
            child: new Text('Continue',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validateAndSubmit,
          ),
        ));
  }

  Widget showRecoveryTextField() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: _smsOptionSelectd ? 
          showNumberInput() : _emailOptionSelectd ? showEmailInput() : new Container(height: 0.0,)
        ));
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Your email address',
            ),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

Widget showNumberInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Your registered phone number',
            ),
        validator: (value) => value.isEmpty ? 'Number can\'t be empty' : null,
        onSaved: (value) => _phoneNumber = value.trim(),
      ),
    );
  }

  _goNext(String verifCode) async {
    try {
      Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => 
              RecoverCodePage(auth: widget.auth,verifCode : verifCode)
              )
            );
    } catch (e) {
      print(e);
    }
  }

  _goBack() async {
    try {
      Navigator.pop(context);
      //widget.loginCallback();
    } catch (e) {
      print(e);
    }
  }

}