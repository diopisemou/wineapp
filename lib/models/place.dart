import 'package:cloud_firestore/cloud_firestore.dart';

/***
 * Category => ensemble de place
 * ajouter key du category
 * 
 */
class Place {
  final String key;
  final String color1;
  final String color2;
  final String name;
  final List<String> img;
  final String price;
  final String etat;
  final String libele;
  final String location;
  final String details;
  final String keyCategory;
  final bool hasTerasse;
  final bool hasWifi;
  final bool hasParking;
  final bool hasResevation;
  final bool hasFumeur;
  final String telephone;
  final double rate;
  final String webSite;
  final String heureOuverture;
  final String heureFermeture;

  Place(
    this.key,
    this.color1,
    this.color2,
    this.name,
    this.img,
    this.price,
    this.etat,
    this.libele,
    this.location,
    this.details,
    this.keyCategory,
    this.hasTerasse,
    this.hasWifi,
    this.hasParking,
    this.hasResevation,
    this.hasFumeur,
    this.telephone,
    this.rate,
    this.webSite,
    this.heureOuverture,
    this.heureFermeture,
  );

  Place.optional({
    this.key,
    this.color1,
    this.color2,
    this.name,
    this.img,
    this.price,
    this.etat,
    this.libele,
    this.location,
    this.details,
    this.keyCategory,
    this.hasTerasse,
    this.hasWifi,
    this.hasParking,
    this.hasResevation,
    this.hasFumeur,
    this.telephone,
    this.rate,
    this.webSite,
    this.heureOuverture,
    this.heureFermeture,
  });

  Place.fromSnapshot(QueryDocumentSnapshot snapshot)
      : key = snapshot.data()["key"],
        color1 = snapshot.data()["color1"],
        color2 = snapshot.data()["color2"],
        name = snapshot.data()["name"],
        img = snapshot.data()["img"],
        price = snapshot.data()["price"],
        etat = snapshot.data()["etat"],
        libele = snapshot.data()["libele"],
        location = snapshot.data()["location"],
        details = snapshot.data()["datails"],
        keyCategory = snapshot.data()["keyCategory"],
        hasTerasse = snapshot.data()["hasTerasse"],
        hasWifi = snapshot.data()["hasWifi"],
        hasParking = snapshot.data()["hasParking"],
        hasResevation = snapshot.data()["hasResevation"],
        hasFumeur = snapshot.data()["hasFumeur"],
        telephone = snapshot.data()['telephone'],
        rate = snapshot.data()['rate'],
        webSite = snapshot.data()['webSite'],
        heureOuverture = snapshot.data()['heureOuverture'],
        heureFermeture = snapshot.data()['heureFermeture'];

/*   Place.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        color1 = snapshot.value["color1"],
        color2 = snapshot.value["color2"],
        name = snapshot.value["name"],
        img = snapshot.value["img"],
        price = snapshot.value["price"],
        location = snapshot.value["location"],
        keyCategory = snapshot.value["keyCategory"]
        ;
 */

  Place copyWith({
    String key,
    String color1,
    String color2,
    String name,
    List<String> img,
    String price,
    String etat,
    String libele,
    String location,
    String details,
    String keyCategory,
    bool hasTerasse,
    bool hasWifi,
    bool hasParking,
    bool hasResevation,
    bool hasFumeur,
    String telephone,
    double rate,
    String webSite,
    String heureOuverture,
    String heureFermeture,
  }) {
    return Place(
      key ?? this.key,
      color1 ?? this.color1,
      color2 ?? this.color2,
      name ?? this.name,
      img ?? this.img,
      price ?? this.price,
      etat ?? this.etat,
      libele ?? this.libele,
      location ?? this.location,
      details ?? this.details,
      keyCategory ?? this.keyCategory,
      hasTerasse ?? this.hasTerasse,
      hasWifi ?? this.hasWifi,
      hasParking ?? this.hasParking,
      hasResevation ?? this.hasResevation,
      hasFumeur ?? this.hasFumeur,
      telephone ?? this.telephone,
      rate ?? this.rate,
      webSite ?? this.webSite,
      heureOuverture ?? this.heureOuverture,
      heureFermeture ?? this.heureFermeture,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'color1': color1,
      'color2': color2,
      'name': name,
      'img': img,
      'price': price,
      'etat': etat,
      'libele': libele,
      'location': location,
      'details': details,
      'keyCategory': keyCategory,
      'hasTerasse': hasTerasse,
      'hasWifi': hasWifi,
      'hasParking': hasParking,
      'hasResevation': hasResevation,
      'hasFumeur': hasFumeur,
      'webSite': webSite,
      'heureOuverture': heureOuverture,
      'heureFermeture': heureFermeture,
    };
  }
}
