import 'package:flutter/material.dart';
import 'package:wineapp/pages/confirmpassword_page.dart';
import 'package:wineapp/pages/login_page.dart';
import 'package:wineapp/pages/passwordconfirmation_page.dart';
import 'package:wineapp/pages/recovercode_page.dart';
import 'package:wineapp/pages/signup_page.dart';
import 'package:wineapp/pages/recoverpassword_page.dart';
import 'package:wineapp/services/authentication.dart';
import 'package:wineapp/pages/home_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  FORGOT_PASSWORD,
  CHANGE_PASSWORD,
  RECOVER_CODE,
  RECOVER_CODE_SUCCESS,
  SIGNING_UP,
  LOGGED_IN,
}

class RootPage extends StatefulWidget {
  
  

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";
  String verifCode = "";

  BaseAuth _baseAuth ;

  @override
  void initState() {
    _baseAuth = context.read<BaseAuth>();
    _baseAuth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user?.uid;
        }
        authStatus =
            user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
    super.initState();
  }

  void loginCallback() {
    _baseAuth.getCurrentUser().then((user) {
      if (user != null) {
        setState(() {
          _userId = user.uid.toString();
          authStatus = AuthStatus.LOGGED_IN;
        });
      } else {
        setState(() {
          authStatus = AuthStatus.NOT_LOGGED_IN;
        });
      }
    });
  }

  void signUpCallback() {
    setState(() {
      authStatus = AuthStatus.SIGNING_UP;
    });
  }

  void forgotPasswordCallback() {
    setState(() {
      authStatus = AuthStatus.FORGOT_PASSWORD;
    });
  }

  void changePasswordCallback() {
    setState(() {
      authStatus = AuthStatus.CHANGE_PASSWORD;
    });
  }

  void changePasswordConfirmationCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
    });
  }

  void recoverCodeCallback(_verifCode) {
    setState(() {
      authStatus = AuthStatus.RECOVER_CODE;
      this.verifCode = _verifCode;
    });
  }

  void passwordChangeCallback(_verifCode) {
    setState(() {
      authStatus = AuthStatus.RECOVER_CODE_SUCCESS;
    });
  }

  void recoverCodeSuccessCallback(_verifCode) {
    setState(() {
      authStatus = AuthStatus.RECOVER_CODE_SUCCESS;
      this.verifCode = verifCode;
    });
  }

  void logoutCallback() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
    });
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return new LoginPage();
        break;
      case AuthStatus.SIGNING_UP:
        return SignUpPage(auth: context.read<BaseAuth>(), loginCallback: loginCallback);
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          return new HomePage(
            userId: _userId,
            auth: context.read<BaseAuth>(),
            logoutCallback: logoutCallback,
          );
        } else
          return buildWaitingScreen();
        break;
      default:
        return buildWaitingScreen();
    }
  }

  // @override
  // Widget build(BuildContext context) {
  //   switch (authStatus) {
  //     case AuthStatus.NOT_DETERMINED:
  //       return buildWaitingScreen();
  //       break;
  //     case AuthStatus.NOT_LOGGED_IN:
  //       return new LoginPage(
  //         auth: widget.auth,
  //         loginCallback: loginCallback,
  //         signUpCallback: signUpCallback,
  //         forgotPasswordCallback: forgotPasswordCallback,
  //       );
  //       break;
  //     case AuthStatus.SIGNING_UP:
  //       return new SignUpPage(
  //         auth: widget.auth,
  //         loginCallback: loginCallback,
  //       );
  //       break;
  //     case AuthStatus.FORGOT_PASSWORD:
  //       return new RecoverPasswordPage(
  //         auth: widget.auth,
  //         recoverCodeCallback: recoverCodeCallback,
  //       );
  //       break;
  //     case AuthStatus.RECOVER_CODE:
  //       return new RecoverCodePage(
  //         auth: widget.auth,
  //         verifCode : this.verifCode,
  //         recoverCodeSuccessCallback: recoverCodeSuccessCallback,
  //       );
  //       break;
  //     case AuthStatus.RECOVER_CODE_SUCCESS:
  //       return new ConfirmPasswordPage(
  //         auth: widget.auth,
  //         verifCode : verifCode,
  //         changePasswordCallback: changePasswordCallback,
  //       );
  //       break;
  //     case AuthStatus.CHANGE_PASSWORD:
  //       return new PasswordConfirmationPage(
  //         auth: widget.auth,
  //         changePasswordConfirmationCallback: changePasswordConfirmationCallback,
  //       );
  //       break;
  //     case AuthStatus.LOGGED_IN:
  //       if (_userId.length > 0 && _userId != null) {
  //         return new HomePage(
  //           userId: _userId,
  //           auth: widget.auth,
  //           logoutCallback: logoutCallback,
  //         );
  //       } else
  //         return buildWaitingScreen();
  //       break;
  //     default:
  //       return buildWaitingScreen();
  //   }
  // }
}
