import 'package:wineapp/models/evenement.dart';

abstract class EvenementService {
  Future<List<Evenement>> evenements({int page});
}
