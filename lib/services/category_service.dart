import 'package:wineapp/models/category.dart';

abstract class CategoryService {
  Future<void> updateFavoriteCategories(List<Category> categories);
  Stream<List<Category>> categories();
  Stream<List<Category>> favoriteCategories();
}
