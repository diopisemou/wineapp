import 'package:flutter/material.dart';
import 'package:wineapp/components/common_scaffold.dart';

class BlancPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CommonScaffold(
      appTitle: "",
      showFAB: false,
      showDrawer: false,
      floatingIcon: null,
      showBottomNav: false,
      bodyData: SingleChildScrollView(
        child: Container(width: double.infinity, color: Colors.orange),
      ),
    );
  }
}
