import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:wineapp/models/evenement.dart';
import 'package:wineapp/services/evenement_service.dart';

part 'evenement_event.dart';
part 'evenement_state.dart';

class EvenementBloc extends Bloc<EvenementEvent, EvenementState> {
  final EvenementService _evenementService;
  EvenementBloc({@required EvenementService evenementService})
      : assert(evenementService != null),
        this._evenementService = evenementService,
        super(EvenementInitial());

  @override
  Stream<EvenementState> mapEventToState(
    EvenementEvent event,
  ) async* {
    if (event is LoadEvenements) {
      yield* _mapLoadEvenementsToState(event);
    }
  }

  Stream<EvenementState> _mapLoadEvenementsToState(
      LoadEvenements event) async* {
    final events = await _evenementService.evenements();
    yield EvenementLoaded(events);
  }
}
