import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/evenement/evenement_bloc.dart';
import 'package:wineapp/models/evenement.dart';
import 'package:shimmer/shimmer.dart';

class CarouselEvenement extends StatefulWidget {
  const CarouselEvenement({Key key}) : super(key: key);

  @override
  _CarouselEvenementState createState() => _CarouselEvenementState();
}

class _CarouselEvenementState extends State<CarouselEvenement> {
  @override
  void initState() {
    super.initState();
    context.read<EvenementBloc>().add(LoadEvenements());
  }

  @override
  Widget build(BuildContext context) {
    
    return Container(
      padding: EdgeInsets.only(top: 10, left: 20),
      height: 178,
      width: MediaQuery.of(context).size.width,
      child: BlocBuilder<EvenementBloc, EvenementState>(
        builder: (context, state) {
          if (state is EvenementLoaded) {
            final events = state.evenements;
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              primary: false,
              itemCount: events.isEmpty ? 0 : events.length,
              itemBuilder: (_, index) => CarouselItemEvenement(
                evenement: events.elementAt(index),
              ),
            );
          }
          return ListView.builder(
            scrollDirection: Axis.horizontal,
            primary: false,
            itemCount: 3,
            itemBuilder: (__, index) => Shimmer.fromColors(
              child: CarouselItemEvenement(evenement: null),
              baseColor: Colors.white10,
              highlightColor: Colors.white70,
            ),
          );
        },
      ),
    );
  }
}

class CarouselItemEvenement extends StatelessWidget {
  const CarouselItemEvenement({
    Key key,
    @required this.evenement,
  }) : super(key: key);

  final Evenement evenement;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: InkWell(
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              /** Prendre au hasard une image */
              child: evenement == null
                  ? Container(
                      width: 260,
                      height: 178,
                      color: Colors.grey,
                    )
                  : Container(
                      height: 178,
                      width: 260,
                      //margin: const EdgeInsets.only(left: 6),
                      decoration: new BoxDecoration(
                        /* borderRadius: new BorderRadius.only(
                          topLeft: new Radius.circular(5.0),
                          topRight: new Radius.circular(5.0),
                          bottomLeft: new Radius.circular(5.0),
                          bottomRight: new Radius.circular(5.0),
                        ), */
                        image: new DecorationImage(
                          image: NetworkImage(evenement.urlImage),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.black12,
                        Colors.black87,
                      ],
                    ),
                  ),
                  child: evenement == null
                    ? Container(
                        width: 260,
                        height: 8.0,
                        color: Colors.grey,
                      )
                    : Text(
                    evenement.libele,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
