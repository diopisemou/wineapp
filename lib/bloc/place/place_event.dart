part of 'place_bloc.dart';

abstract class PlaceEvent extends Equatable {
  const PlaceEvent();

  @override
  List<Object> get props => [];
}

class LoadPlaces extends PlaceEvent {
  @override
  String toString() => "LoadPlaces";
}

class LoadPlacesForCategory extends PlaceEvent {
  final String keyCategory;

  LoadPlacesForCategory(this.keyCategory);
  @override
  String toString() => "LoadPlacesForCategory";
}

