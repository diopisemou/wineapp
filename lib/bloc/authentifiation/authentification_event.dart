part of 'authentification_bloc.dart';

@immutable
abstract class AuthentificationEvent {}

class AppStarted extends AuthentificationEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthentificationEvent {
  final String login;
  final String password;

  LoggedIn({@required this.login, @required this.password});

  @override
  String toString() => 'LoggedIn(login: $login, password: $password)';
}

class LoggedOut extends AuthentificationEvent {
  @override
  String toString() => 'LoggedOut';
}
