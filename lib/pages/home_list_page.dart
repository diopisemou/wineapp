import 'package:flutter/material.dart';
import 'package:wineapp/pages/home_list.dart';

class HomeListPage extends StatelessWidget {
  const HomeListPage({Key key}) : super(key: key);


void moveTo() {
    // Navigator.push<dynamic>(
    //   context,
    //   MaterialPageRoute<dynamic>(
    //     builder: (BuildContext context) => CourseInfoScreen(),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Image.asset('assets/icon/ic_launcher.png'),
          elevation: 0,
          backgroundColor: Colors.transparent,
          brightness: Brightness.light,
          actions: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 32, right: 32),
              height: 80,
              child: Container(
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  //color: Color(0x50F02D2D),
                  color: Colors.white
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 8, right: 16, top: 10),
                      child: Icon(
                        Icons.menu,
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),

        bottomNavigationBar: BottomAppBar(
          color: Colors.black,
          shape: CircularNotchedRectangle(),
          child: Container(
            color: Colors.white,
            // height: 45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.home,
                          color: Colors.black,
                          // size: 32,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Accueil',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        )
                      ],
                    ),
                    // onPressed: () {},
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.favorite,
                          color: Colors.black,
                          // size: 32,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Explore',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        )
                      ],
                    ),
                    // onPressed: () {},
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.bookmark,
                          color: Colors.black,
                          // size: 32,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Enregistre',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        )
                      ],
                    ),
                    // onPressed: () {},
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: Colors.black,
                          // size: 32,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Profil',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        )
                      ],
                    ),
                    // onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ),
        body: HomeList(
              callBack: () {
                moveTo();
              },
            ),
          
      ),
    );
  }
}