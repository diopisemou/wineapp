import 'package:wineapp/models/notification.dart';

abstract class NotificationService {
  Stream<List<MyNotification>> notifications();
  Future<void> deleteNotification(MyNotification notification);
  Future<void> visit(MyNotification notification);
}
