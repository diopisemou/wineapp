part of 'notification_bloc.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => [];
}

class LoadNotification extends NotificationEvent {
  @override
  String toString() => "LoadNotification";
}

class VisitNotification extends NotificationEvent {
  final MyNotification notification;

  VisitNotification(this.notification);

  @override
  String toString() => 'VisitNotification(notification: $notification)';
}

class DeleteNotification extends NotificationEvent {
  final MyNotification notification;

  DeleteNotification(this.notification);
  @override
  String toString() => 'DeleteNotification(notification: $notification)';
}

class NotificationsUpdated extends NotificationEvent {
  final List<MyNotification> notifications;

  NotificationsUpdated(this.notifications);

  @override
  String toString() => 'NotificationsUpdated(notifications: $notifications)';
}
