
import 'package:cloud_firestore/cloud_firestore.dart';

class Evenement {
  final String key;
  final String libele;
  final String urlImage;

  Evenement(this.libele, this.key, this.urlImage);
  Evenement.fromSnapshot(QueryDocumentSnapshot snapshot)
      : key = snapshot.id,
        libele = snapshot.data()["libele"],
        urlImage = snapshot.data()['urlImage'];

  Evenement.optonal({
    this.libele,
    this.key,
    this.urlImage,
  });

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'libele': libele,
      'urlImage': urlImage,
    };
  }

}
