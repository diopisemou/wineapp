import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

@immutable
class Category extends Equatable {
  final String key;
  final String color1;
  final String color2;
  final String name;
  final String img;
  final String price;
  final String location;
  final String details;

  Category(this.key, this.color1, this.color2, this.name, this.img, this.price,
      this.location, this.details);

  Category.fromSnapshot(QueryDocumentSnapshot snapshot)
      : key = snapshot.id,
        color1 = snapshot.data()["color1"],
        color2 = snapshot.data()["color2"],
        name = snapshot.data()["name"],
        img = snapshot.data()["img"],
        price = snapshot.data()["price"],
        location = snapshot.data()["location"],
        details = snapshot.data()["details"];
/*   Category.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        color1 = snapshot.value["color1"],
        color2 = snapshot.value["color2"],
        name = snapshot.value["name"],
        img = snapshot.value["img"],
        price = snapshot.value["price"],
        location = snapshot.value["location"],
        details = snapshot.value["details"]; */

  Category copyWith({
    String key,
    String color1,
    String color2,
    String name,
    String img,
    String price,
    String location,
    String details,
  }) {
    return Category(
      key ?? this.key,
      color1 ?? this.color1,
      color2 ?? this.color2,
      name ?? this.name,
      img ?? this.img,
      price ?? this.price,
      location ?? this.location,
      details ?? this.details,
    );
  }

  @override
  List<Object> get props => [
        this.key,
        this.color1,
        this.color2,
        this.name,
        this.img,
        this.price,
        this.location,
        this.details
      ];

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'color1': color1,
      'color2': color2,
      'name': name,
      'img': img,
      'price': price,
      'location': location,
      'details': details,
    };
  }
}
