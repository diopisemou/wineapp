part of 'evenement_bloc.dart';

abstract class EvenementState extends Equatable {
  const EvenementState();

  @override
  List<Object> get props => [];
}

class EvenementInitial extends EvenementState {}

class EvenementLoaded extends EvenementState {
  final List<Evenement> evenements;

  EvenementLoaded(this.evenements);
  List<Object> get props => [evenements];
  @override
  String toString() => "EvenementLoaded";
}
