import 'package:flutter/material.dart';
import 'package:wineapp/bloc/profil/profil_bloc.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/shared.dart';
import 'package:wineapp/widgets/custom_profil_pass.dart';
import 'package:wineapp/widgets/form_email.dart';
import 'package:wineapp/widgets/form_full_name.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Compte extends StatefulWidget {
  @override
  _CompteState createState() => _CompteState();
}

class _CompteState extends State<Compte> {
  String fullName = "mohamed";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Compte'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: [
          CompteTopPart(),
          Expanded(
            child: BlocConsumer<ProfilBloc, ProfilState>(
              listener: (_, state) {
                if (state is ProfilUpdated) {
                  Fluttertoast.showToast(
                    msg: state.message,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: state.updateStatus == UpdateStatus.success
                        ? Colors.blue
                        : Colors.yellow,
                    textColor: Colors.white,
                    fontSize: 16.0,
                  );
                }
              },
              buildWhen: (prevState, currentState) => currentState is ProfilDisplayed,
              builder: (context, state) {
                if (state is ProfilDisplayed) {
                  return ListView(
                    children: [
                      ListTileProfil(
                        icon: Icon(Icons.account_circle, color: Colors.blue),
                        label: 'Nom complet',
                        subTitle:
                            "Ceci correspond à notre nom qui sera visible par les autres utilisateurs de l'application",
                        value: state.fullName,
                        onTap: () => _onUpdateProfil(
                          child: Container(
                            child: FormFullName(
                              currentValue: state.fullName,
                              onSave: (value) {
                                context
                                    .read<ProfilBloc>()
                                    .add(UpdateFullName(fullName: value));
                              },
                            ),
                          ),
                        ),
                      ),
                      ListTileProfil(
                        icon: Icon(Icons.account_circle, color: Colors.blue),
                        label: 'adresse email',
                        subTitle: "Ceci correspond à notre adresse email",
                        value: state.email,
                        onTap: () => _onUpdateProfil(
                          child: Container(
                            child: FormEmail(
                              currentValue: state.email,
                              onSave: (email) => context
                                  .read<ProfilBloc>()
                                  .add(UpdateEmail(email: email)),
                            ),
                          ),
                        ),
                      ),
                      Divider(),
                      ListTile(
                        leading: Icon(Icons.security, color: Colors.blue),
                        title: Text(
                          'Changer le mot de passe',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                          ),
                        ),
                        onTap: () {
                          context
                              .read<ProfilBloc>()
                              .add(DisplayChangePassWord());
                          _onUpdateProfil(
                            child: Container(
                              /*  height: MediaQuery.of(context).viewInsets.bottom +
                                  350, */
                              /*  constraints: BoxConstraints(
                                  maxHeight:
                                      MediaQuery.of(context).size.height - 50), */
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        'Changer mot de passe',
                                        style: TextStyle(
                                            color: Colors.orange[900],
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                      FlatButton(
                                        /* padding:
                                                EdgeInsets.fromLTRB(5, 0, 2, 0), */
                                        // color: Colors.grey[100],
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30),
                                        ),
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        child: Icon(
                                          Icons.close,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  FormPasseWord(
                                    onSave: (value) {
                                      //context.read<ProfilBloc>().add(UpdatePasseWord(passWord: value));
                                    },
                                  ),
                                  /*  Expanded(
                                        child: Scaffold(
                                          resizeToAvoidBottomPadding: true,
                                          body: FormPasseWord(
                                            scrollController: scrollController,
                                            onSave: (value) {
                                              //context.read<ProfilBloc>().add(UpdatePasseWord(passWord: value));
                                            },
                                          ),
                                        ),
                                      ), */
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  );
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  _onUpdateProfil({@required Widget child}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15.0),
          topRight: Radius.circular(15.0),
        ),
      ),
      builder: (_) => SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 14, horizontal: 26),
          child: BlocProvider.value(
            value: context.read<ProfilBloc>(),
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}

class CompteTopPart extends StatefulWidget {
  @override
  _CompteTopPartState createState() => _CompteTopPartState();
}

class _CompteTopPartState extends State<CompteTopPart> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            ClipPath(
              child: Container(
                height: screenHeight * .25,
                width: screenWidth,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xffF5D020), Color(0xffF53803)],
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: screenWidth,
                      height: screenHeight * .25,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            margin: EdgeInsets.only(bottom: screenHeight * .04),
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.1),
                                borderRadius: BorderRadius.circular(25)),
                            child: Icon(
                              Icons.settings_applications,
                              size: 50,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              clipper: CostumShapeClipper(),
            ),
            Positioned(
              top: screenHeight * .08,
              left: 0,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  "Mon compte",
                  style: TextStyle(
                      color: Colors.blue[600],
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class CostumShapeClipper extends CustomClipper<Path> {
  Path getClip(Size size) {
    Path path = Path();
    final double _xScaling = size.width / 374.157;
    final double _yScaling = size.height / 187.908;
    path.lineTo(0 * _xScaling, 187.90600000000006 * _yScaling);
    path.cubicTo(
      0 * _xScaling,
      187.90600000000006 * _yScaling,
      374.157 * _xScaling,
      187.90600000000006 * _yScaling,
      374.157 * _xScaling,
      187.90600000000006 * _yScaling,
    );
    path.cubicTo(
      374.157 * _xScaling,
      187.90600000000006 * _yScaling,
      374.157 * _xScaling,
      0.03300000000001546 * _yScaling,
      374.157 * _xScaling,
      0.03300000000001546 * _yScaling,
    );
    path.cubicTo(
      374.157 * _xScaling,
      0.03300000000001546 * _yScaling,
      294.549 * _xScaling,
      -3.1510000000000105 * _yScaling,
      243.6 * _xScaling,
      52.57400000000001 * _yScaling,
    );
    path.cubicTo(
      192.651 * _xScaling,
      108.29900000000004 * _yScaling,
      194.243 * _xScaling,
      105.11500000000001 * _yScaling,
      168.769 * _xScaling,
      119.445 * _yScaling,
    );
    path.cubicTo(
      143.29500000000002 * _xScaling,
      133.77499999999998 * _yScaling,
      52.541 * _xScaling,
      154.47300000000007 * _yScaling,
      52.541 * _xScaling,
      154.47300000000007 * _yScaling,
    );
    path.cubicTo(
      52.541 * _xScaling,
      154.47300000000007 * _yScaling,
      0 * _xScaling,
      162.433 * _yScaling,
      0 * _xScaling,
      162.433 * _yScaling,
    );
    path.cubicTo(
      0 * _xScaling,
      162.433 * _yScaling,
      0 * _xScaling,
      187.90600000000006 * _yScaling,
      0 * _xScaling,
      187.90600000000006 * _yScaling,
    );
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) => true;
}

class ListTileProfil extends StatelessWidget {
  final String label;
  final String value;
  final String subTitle;
  final Function onTap;
  final bool separated;
  final Widget icon;
  ListTileProfil(
      {Key key,
      @required this.label,
      @required this.value,
      this.icon,
      this.subTitle,
      this.separated = true,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('$label',
              style: TextStyle(color: Colors.grey[700], fontSize: 15)),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '$value',
                style: TextStyle(color: Colors.black),
              ),
              Icon(Icons.edit, color: Colors.grey)
            ],
          ),
          SizedBox(height: 15),
          subTitle != null
              ? Text(
                  "$subTitle",
                  style: TextStyle(color: Colors.grey[700], fontSize: 15),
                  textAlign: TextAlign.justify,
                )
              : Container(),
          SizedBox(height: 1),
          separated ? Divider() : Container(),
        ],
      ),
      onTap: onTap,
    );
  }
}
