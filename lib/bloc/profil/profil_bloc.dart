import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:wineapp/services/authentication.dart';

import '../shared.dart';

part 'profil_event.dart';
part 'profil_state.dart';

class ProfilBloc extends Bloc<ProfilEvent, ProfilState> {
  final BaseAuth _baseAuth;
  String _fullName = 'mohamed';
  String _email = 'mohamed@gmail.com';
  ProfilBloc({@required BaseAuth baseAuth})
      : assert(baseAuth != null),
        this._baseAuth = baseAuth,
        super(ProfilInitial());

  @override
  Stream<ProfilState> mapEventToState(
    ProfilEvent event,
  ) async* {
    if (event is DisplayProfil) {
      yield* _mapDisplayProfilToState(event);
    } else if (event is UpdateFullName) {
      yield* _mapUpdateFullNameToState(event);
    } else if (event is UpdateEmail) {
      yield* _mapUpdateEmailToState(event);
    } else if (event is DisplayChangePassWord) {
      yield* _mapDisplayChangePassWordToState(event);
    } else if (event is UpdatePassword) {
      yield* _mapUpdatePasswordToState(event);
    }
  }

  Stream<ProfilState> _mapUpdateFullNameToState(UpdateFullName event) async* {
    try {
      _fullName = event.fullName;
      _baseAuth.updteFullName(event.fullName);
      yield ProfilUpdated(
        message: "Nom mis à jour avec succès",
        updateStatus: UpdateStatus.success,
      );
      yield ProfilDisplayed(
        email: _email,
        fullName: _fullName,
      );
    } catch (e) {
      yield ProfilUpdated(
        message: "erreur lors de la mise à jour",
        updateStatus: UpdateStatus.failed,
      );
    }
  }

  Stream<ProfilState> _mapUpdateEmailToState(UpdateEmail event) async* {
    //TODO: mise à jour de l'email
    //_baseAuth.updteFullName(event.email);
    _email = event.email;
    yield ProfilDisplayed(email: _email, fullName: _fullName);
  }

  Stream<ProfilState> _mapDisplayProfilToState(DisplayProfil event) async* {
    //final user = await _baseAuth.getCurrentUser();
    // TODO: recuperer l'utilisateur courant
    yield ProfilDisplayed(email: _email, fullName: _fullName);
  }

  Stream<ProfilState> _mapUpdatePasswordToState(UpdatePassword event) async* {
    // TODO: vérifier ancien mot de passe l'utilisateur courant
    try {
      //_baseAuth.updatePassword(event.newPassword);
      yield ((state as ProfilPasswordState).copyWith(
          formSubmittedSuccessfully: false, isIncorrectPassword: true));
      /* yield ProfilUpdated(
        message: "mot de passe mis à jour avec succès",
        updateStatus: UpdateStatus.success,
      ); */
    } catch (e) {
      yield (state as ProfilPasswordState)
          .copyWith(formSubmittedSuccessfully: false);
      yield ProfilUpdated(
        message: "Erreur lors de la mise à jour",
        updateStatus: UpdateStatus.failed,
      );
    }
  }

  Stream<ProfilState> _mapDisplayChangePassWordToState(
      DisplayChangePassWord event) async* {
    yield ProfilPasswordState();
  }
}
