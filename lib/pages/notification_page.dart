import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';
import 'package:wineapp/bloc/notification/notification_bloc.dart';
import 'package:wineapp/utils/extension.dart';
import 'package:wineapp/models/notification.dart';
import 'package:timeago/timeago.dart' as timeago;

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  void initState() {
    super.initState();
    timeago.setLocaleMessages('fr', timeago.FrMessages());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Notifications'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, state) {
        if (state is NotificationLoaded) {
          return ListView(
            children: [
              ...(state.notifications.mapIndexed(
                (notif, index) => Card(
                  child: Dismissible(
                    key: Key('${notif.title}$index'),
                    direction: DismissDirection.endToStart,
                    onDismissed: (direction) {
                      context
                          .read<NotificationBloc>()
                          .add(DeleteNotification(notif));
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text("${notif.title} dismissed")));
                    },
                    background: Container(
                      color: Colors.red,
                      alignment: AlignmentDirectional.centerEnd,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    child: InkWell(
                      child: NotificationItem(notification: notif),
                    ), /* ListTile(
                      title: Text(notif.title),
                      subtitle: Text("${notif.message}"),
                      trailing: Text("${timeago.format(notif.date, locale: 'fr')}"),
                    ), */
                  ),
                ),
              )),
              SizedBox(
                height: 100,
              )
            ],
          );
        }

        return ListView.builder(
          scrollDirection: Axis.vertical,
          primary: false,
          itemCount: 50,
          itemBuilder: (_, index) => Shimmer.fromColors(
            child: NotificationItem(),
            baseColor: Colors.white,
            highlightColor: Colors.grey[200],
          ),
        );
      }),
    );
  }
}

class NotificationItem extends StatelessWidget {
  final MyNotification notification;
  const NotificationItem({
    Key key,
    this.notification,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: notification == null
          ? EdgeInsets.symmetric(horizontal: 8)
          : EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          notification == null
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 8),
                  height: 15,
                  width: 50,
                  color: Colors.white,
                )
              : Text(
                  notification.title,
                  style: TextStyle(fontSize: 20),
                ),
          notification == null
              ? Column(
                  children: [
                    Container(
                      height: 8,
                      width: double.infinity,
                      color: Colors.white,
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 8,
                      width: double.infinity,
                      color: Colors.white,
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 8,
                      width: double.infinity,
                      color: Colors.white,
                    ),
                  ],
                )
              : Text(
                  notification.message,
                  style: TextStyle(fontSize: 17),
                ),
          Row(
            children: [
              Container(
                width: 5,
                height: 5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.grey[400],
                ),
              ),
              SizedBox(width: 5),
              notification == null
                  ? Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      height: 8,
                      width: 80,
                      color: Colors.white,
                    )
                  : Text(
                      "${timeago.format(notification.date, locale: 'fr')}",
                      style: TextStyle(fontSize: 14),
                    ),
            ],
          ),
        ],
      ),
    );
  }
}
