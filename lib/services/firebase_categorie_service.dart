import 'package:wineapp/models/category.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'category_service.dart';

class FireBaseCategoryService implements CategoryService {
  final CollectionReference categoryCollection =
      FirebaseFirestore.instance.collection('category');

  @override
  Stream<List<Category>> categories() {
    return categoryCollection.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) => Category.fromSnapshot(doc)).toList();
    });
  }

  @override
  Stream<List<Category>> favoriteCategories() {
    throw UnimplementedError();
  }

  @override
  Future<void> updateFavoriteCategories(List<Category> categories) async {
    throw UnimplementedError();
  }
}
