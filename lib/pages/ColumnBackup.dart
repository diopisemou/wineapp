// Column(
//               children: <Widget>[
//                 AspectRatio(
//                   aspectRatio: 1.2,
//                   child: Image.asset('assets/images/webInterFace.png'),
//                 ),
//               ],
//             ),
//             Positioned(
//               top: (MediaQuery.of(context).size.width / 1.2) - 24.0,
//               bottom: 0,
//               left: 0,
//               right: 0,
//               child: Container(
//                 decoration: BoxDecoration(
//                   color: AppTheme.nearlyWhite,
//                   borderRadius: const BorderRadius.only(
//                       topLeft: Radius.circular(32.0),
//                       topRight: Radius.circular(32.0)),
//                   boxShadow: <BoxShadow>[
//                     BoxShadow(
//                         color: AppTheme.grey.withOpacity(0.2),
//                         offset: const Offset(1.1, 1.1),
//                         blurRadius: 10.0),
//                   ],
//                 ),
//                 child: Padding(
//                   padding: const EdgeInsets.only(left: 8, right: 8),
//                   child: SingleChildScrollView(
//                     child: Container(
//                       constraints: BoxConstraints(
//                           minHeight: infoHeight,
//                           maxHeight: tempHeight > infoHeight
//                               ? tempHeight
//                               : infoHeight),
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: <Widget>[
//                           Padding(
//                             padding: const EdgeInsets.only(
//                                 top: 32.0, left: 18, right: 16),
//                             child: Text(
//                               'Web Design\nCourse',
//                               textAlign: TextAlign.left,
//                               style: TextStyle(
//                                 fontWeight: FontWeight.w600,
//                                 fontSize: 22,
//                                 letterSpacing: 0.27,
//                                 color: AppTheme.darkerText,
//                               ),
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.only(
//                                 left: 16, right: 16, bottom: 8, top: 16),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               crossAxisAlignment: CrossAxisAlignment.center,
//                               children: <Widget>[
//                                 Text(
//                                   '\$28.99',
//                                   textAlign: TextAlign.left,
//                                   style: TextStyle(
//                                     fontWeight: FontWeight.w200,
//                                     fontSize: 22,
//                                     letterSpacing: 0.27,
//                                     color: AppTheme.nearlyBlue,
//                                   ),
//                                 ),
//                                 Container(
//                                   child: Row(
//                                     children: <Widget>[
//                                       Text(
//                                         '4.3',
//                                         textAlign: TextAlign.left,
//                                         style: TextStyle(
//                                           fontWeight: FontWeight.w200,
//                                           fontSize: 22,
//                                           letterSpacing: 0.27,
//                                           color: AppTheme.grey,
//                                         ),
//                                       ),
//                                       Icon(
//                                         Icons.star,
//                                         color: AppTheme.nearlyBlue,
//                                         size: 24,
//                                       ),
//                                     ],
//                                   ),
//                                 )
//                               ],
//                             ),
//                           ),
//                           AnimatedOpacity(
//                             duration: const Duration(milliseconds: 500),
//                             opacity: opacity1,
//                             child: Padding(
//                               padding: const EdgeInsets.all(8),
//                               child: Row(
//                                 children: <Widget>[
//                                   getTimeBoxUI('24', 'Classe'),
//                                   getTimeBoxUI('2hours', 'Time'),
//                                   getTimeBoxUI('24', 'Seat'),
//                                 ],
//                               ),
//                             ),
//                           ),
//                           Expanded(
//                             child: AnimatedOpacity(
//                               duration: const Duration(milliseconds: 500),
//                               opacity: opacity2,
//                               child: Padding(
//                                 padding: const EdgeInsets.only(
//                                     left: 16, right: 16, top: 8, bottom: 8),
//                                 child: Text(
//                                   'Lorem ipsum is simply dummy text of printing & typesetting industry, Lorem ipsum is simply dummy text of printing & typesetting industry.',
//                                   textAlign: TextAlign.justify,
//                                   style: TextStyle(
//                                     fontWeight: FontWeight.w200,
//                                     fontSize: 14,
//                                     letterSpacing: 0.27,
//                                     color: AppTheme.grey,
//                                   ),
//                                   maxLines: 3,
//                                   overflow: TextOverflow.ellipsis,
//                                 ),
//                               ),
//                             ),
//                           ),
//                           AnimatedOpacity(
//                             duration: const Duration(milliseconds: 500),
//                             opacity: opacity3,
//                             child: Padding(
//                               padding: const EdgeInsets.only(
//                                   left: 16, bottom: 16, right: 16),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 crossAxisAlignment: CrossAxisAlignment.center,
//                                 children: <Widget>[
//                                   Container(
//                                     width: 48,
//                                     height: 48,
//                                     child: Container(
//                                       decoration: BoxDecoration(
//                                         color: AppTheme.nearlyWhite,
//                                         borderRadius: const BorderRadius.all(
//                                           Radius.circular(16.0),
//                                         ),
//                                         border: Border.all(
//                                             color: AppTheme.grey
//                                                 .withOpacity(0.2)),
//                                       ),
//                                       child: Icon(
//                                         Icons.add,
//                                         color: AppTheme.nearlyBlue,
//                                         size: 28,
//                                       ),
//                                     ),
//                                   ),
//                                   const SizedBox(
//                                     width: 16,
//                                   ),
//                                   Expanded(
//                                     child: Container(
//                                       height: 48,
//                                       decoration: BoxDecoration(
//                                         color: AppTheme.nearlyBlue,
//                                         borderRadius: const BorderRadius.all(
//                                           Radius.circular(16.0),
//                                         ),
//                                         boxShadow: <BoxShadow>[
//                                           BoxShadow(
//                                               color: AppTheme
//                                                   .nearlyBlue
//                                                   .withOpacity(0.5),
//                                               offset: const Offset(1.1, 1.1),
//                                               blurRadius: 10.0),
//                                         ],
//                                       ),
//                                       child: Center(
//                                         child: Text(
//                                           'Join Course',
//                                           textAlign: TextAlign.left,
//                                           style: TextStyle(
//                                             fontWeight: FontWeight.w600,
//                                             fontSize: 18,
//                                             letterSpacing: 0.0,
//                                             color: AppTheme
//                                                 .nearlyWhite,
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   )
//                                 ],
//                               ),
//                             ),
//                           ),
//                           SizedBox(
//                             height: MediaQuery.of(context).padding.bottom,
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),

//             Positioned(
//               top: (MediaQuery.of(context).size.width / 1.2) - 29.0 ,
//               left: 0,
//               right: 0,
              
//               child: ScaleTransition(
//                 alignment: Alignment.center,
//                 scale: CurvedAnimation(
//                     parent: animationController, curve: Curves.fastOutSlowIn),
//                 child: Card(
//                   color: Colors.orange[300],
//                   shape: RoundedRectangleBorder(
//                       borderRadius: const BorderRadius.only(
//                       topLeft: Radius.circular(32.0),
//                       topRight: Radius.circular(32.0))),
//                   elevation: 10.0,
//                   child: Container(
//                     width: 200,
//                     height: 65,
//                     child: Row (
//                       children: <Widget> [
//                         SizedBox(width: 5,),
//                         Padding(
//                             padding: const EdgeInsets.only(
//                                 top: 2.0, left: 18, right: 16),
//                             child: Column(
//                               children: <Widget>[
//                                 SizedBox(height: 4,),
//                                 Text(
//                                   'Bantu ',
//                                   textAlign: TextAlign.justify,
//                                   style: TextStyle(
//                                     fontWeight: FontWeight.w600,
//                                     fontSize: 22,
//                                     letterSpacing: 0.27,
//                                     color: AppTheme.nearlyWhite,
//                                   ),
//                                 ),
//                                 Text(
//                                   'Restaurant Bar',
//                                   textAlign: TextAlign.end,
//                                   style: TextStyle(
//                                     fontWeight: FontWeight.w600,
//                                     fontSize: 16,
//                                     letterSpacing: 0.27,
//                                     color: AppTheme.nearlyWhite,
//                                   ),
                                  
//                                 ),
//                               ],
//                               )
//                           ),
//                           SizedBox(width: 85,),
//                         Center(
//                           child: Card(
//                             color: AppTheme.white,
//                             shadowColor: Colors.red,
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(50.0)),
//                             elevation: 10.0,
//                             child: Container(
//                               width: 60,
//                               height: 60,
//                               child: Center(
//                                 child: Icon(
//                                   Icons.directions_run,
//                                   color: AppTheme.nearlyBlack,
//                                   size: 30,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Center(
//                           child: Container(
//                               width: 20,
//                               height: 60,
//                               child: Center(
//                                 child: Icon(
//                                   Icons.chevron_right,
//                                   color: AppTheme.white,
//                                   size: 30,
//                                 ),
//                               ),
//                             ),
//                         ),
//                       ]
//                     )
//                   ),
//                 ),
//               ),
//             ),


            


//             Padding(
//               padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
//               child: SizedBox(
//                 width: AppBar().preferredSize.height,
//                 height: AppBar().preferredSize.height,
//                 child: Material(
//                   color: Colors.transparent,
//                   child: InkWell(
//                     borderRadius:
//                         BorderRadius.circular(AppBar().preferredSize.height),
//                     child: Icon(
//                       Icons.arrow_back_ios,
//                       color: AppTheme.nearlyBlack,
//                     ),
//                     onTap: () {
//                       Navigator.pop(context);
//                     },
//                   ),
//                 ),
//               ),
//             )