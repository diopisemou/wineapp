import 'package:flutter/material.dart';
import 'package:wineapp/components/footerads_page.dart';
import 'package:wineapp/constants/constants.dart';
import 'package:wineapp/extensions/HexColor.dart';
import 'package:wineapp/services/authentication.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  String _mobilenumber;
  String _errorMessage;
  bool checkBoxValue = false;
  bool checkBoxValueTristate = false;
  bool passwordVisible = false;

  bool _isLoading;

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup
  void validateAndSubmit() async {
    
    if (validateAndSave()) {
        setState(() {
        _errorMessage = "";
        _isLoading = true;
      });
      String userId = "";
      try {
        userId = await widget.auth.signUpWithNumber(_email, _password, _mobilenumber);
        print('Signed up user: $userId');
        setState(() {
          _isLoading = false;
        });

        if (userId.length > 0 && userId != null ) {
          widget.loginCallback();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          _formKey.currentState.reset();
        });
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    passwordVisible = false;
    super.initState();
  }

  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }


  

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: new FooterAdsPage(adsText:'Advertising 8'),
        appBar: new AppBar(
          backgroundColor: Colors.white,
          leading: new FlatButton(
            child: Icon(
            // Based on passwordVisible state choose the icon
              Icons.arrow_back,
              size: 30.0,
              color: HexColor.fromHex(APP_COLOR_RED),
            ),
            onPressed: _goBack)
        ),
        body: Stack(
          children: <Widget>[
            _showForm(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Container(
      height: 0.0,
      width: 0.0,
    );
      //return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showForm() {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              showPageHeader(),
              showErrorMessage(),
              showEmailLabel(),
              showEmailInput(),
              showPasswordLabel(),
              showPasswordInput(),
              showMobileNumberLabel(),
              showMobileNumberInput(),
              showConditionsButton(),
              showActionButton()
            ],
          ),
        ));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        textAlign : TextAlign.center,
        style: TextStyle(
            fontSize: 15.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showLogo() {
    return new Hero(
      tag: 'herologo',
      child: Padding(
        padding: EdgeInsets.fromLTRB(35.0, 20.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 70.0,
          child: Image.asset('assets/images/KicksTrade.png'),
        ),
      ),
    );
  }


Widget showSignInLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 50.0, 30.0, 0.0),
      child: new Text('Sign Up ', style: new TextStyle(fontSize: 25.0, color: Colors.black, fontWeight: FontWeight.w600),
    ));
  }

  Widget showPageHeader() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
        child: new Column (
          children : <Widget>[
            new Row(
              children: <Widget>[
                showSignInLabel(),
                showLogo()
              ],
          )
          ]
        )
        );
  }



Widget showEmailLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 30.0, 0.0, 0.0),
      child: new Text('Email', style: new TextStyle(fontSize: 20.0, color: HexColor.fromHex(APP_COLOR_RED))),
    );
  }

  Widget showPasswordLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 30.0, 0.0, 0.0),
      child: new Text('Password', style: new TextStyle(fontSize: 20.0, color: HexColor.fromHex(APP_COLOR_RED))),
    );
  }

  Widget showMobileNumberLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 30.0, 0.0, 0.0),
      child: new Text('Mobile Number', style: new TextStyle(fontSize: 20.0, color: HexColor.fromHex(APP_COLOR_RED))),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Your Email Address',
            ),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: TextFormField(
      keyboardType: TextInputType.text,
      maxLines: 1,
      obscureText: passwordVisible,//This will obscure text dynamically
      decoration: InputDecoration(
          hintText: 'Your password',
          // Here is key idea
          suffixIcon: IconButton(
                icon: Icon(
                  // Based on passwordVisible state choose the icon
                  !passwordVisible
                  ? Icons.visibility
                  : Icons.visibility_off,
                  color: Colors.grey[400],
                  ),
                onPressed: () {
                  // Update the state i.e. toogle the state of passwordVisible variable
                  setState(() {
                      passwordVisible = !passwordVisible;
                  });
                },
                ),
              ),
              validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
              onSaved: (value) => _password = value.trim(),
        ),
    );
  }


  Widget showMobileNumberInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: '(###) ###-####',
            ),
        validator: (value) => value.isEmpty ? 'Number can\'t be empty' : null,
        onSaved: (value) => _mobilenumber = value.trim(),
      ),
    );
  }

Widget showConditionsButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
        child: new Row(
          children : <Widget>[
            showTickBoxButton(),
            showConditionsText()
          ]
        )
        );
  }

Widget showActionButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
        child: new Column (
          children : <Widget>[
            showSignUpButton()
          ]
        )
        );
  }


  Widget showSignUpButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
        child: SizedBox(
          height: 60.0,
          width: 140.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8.0)),
            color: HexColor.fromHex(APP_COLOR_RED),
            child: new Text('Sign Up',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validateAndSubmit,
          ),
        ));
  }

  Widget showTickBoxButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(30.0, 20.0, 0.0, 0.0),
        child: SizedBox(
          height: 10.0,
          width: 10.0,
          child: new Checkbox(
            value:checkBoxValue, 
            tristate:true,
            onChanged: (bool newValue) { 
              setState(() {
                checkBoxValue = newValue;
              });
            }, 
           activeColor: HexColor.fromHex(APP_COLOR_RED),
           checkColor: Colors.white,
           autofocus: false,

           ),
        ));
  }

  Widget showConditionsText() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(10.0, 20.0, 0.0, 0.0),
        child: new Row (
          children : <Widget>[
            new Text('I agree to the ', style: new TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.w400)),
            new Text('Terms of Services ', style: new TextStyle(fontSize: 16.0, color: HexColor.fromHex(APP_COLOR_RED), fontWeight: FontWeight.w700)),
            //new Text(' and ', style: new TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.w400)),
            //new Text(' Policy ', softWrap: true, overflow: TextOverflow.fade, style: new TextStyle( fontSize: 15.0, color: HexColor.fromHex(APP_COLOR_RED)))
          ]
        )
        );
  }

_goBack() async {
    try {
      Navigator.pop(context);
      widget.loginCallback();
    } catch (e) {
      print(e);
    }
  }

}