import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/category/category_bloc.dart';
import 'package:wineapp/bloc/evenement/evenement_bloc.dart';
import 'package:wineapp/bloc/place/place_bloc.dart';
import 'package:wineapp/pages/explore.dart';
import 'package:wineapp/pages/home_tab.dart';
import 'package:wineapp/pages/home_list.dart';
import 'package:wineapp/pages/profile_one_page.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wineapp/services/category_service.dart';
import 'package:wineapp/services/evenement_service.dart';
import 'package:wineapp/services/place_service.dart';
import 'package:wineapp/utils/app_theme.dart';

class TabPageContainer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TabPageContainerState();
  }
}

class _TabPageContainerState extends State<TabPageContainer> {
  int _index = 0;
  List<Widget> _widgetList = [];

  CategoryBloc _blocCategory;
  PlaceBloc _blocPlace;
  EvenementBloc _evenementBloc;

  @override
  void initState() {
    super.initState();

    _blocCategory = CategoryBloc(
      categoryService: context.read<CategoryService>(),
    )..add(LoadCategories());

    _blocPlace = PlaceBloc(
      placeService: context.read<PlaceService>(),
    )..add(LoadPlaces());

    _evenementBloc = EvenementBloc(
      evenementService: context.read<EvenementService>(),
    )..add(LoadEvenements());

    _widgetList = [
      HomeTab(
        goToEnregister: () {
          setState(() {
            _index = 2;
          });
          // moveTo();
        },
      ),
      Explore(
        callBack: () {
          moveTo();
        },
      ),
      HomeList(
        callBack: () {
          moveTo();
        },
      ),
      ProfileOnePage(),
    ];
  }

  static void moveTo() {
    // Navigator.push<dynamic>(
    //   context,
    //   MaterialPageRoute<dynamic>(
    //     builder: (BuildContext context) => CourseInfoScreen(),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.black,
          type: BottomNavigationBarType.fixed,
          currentIndex: _index,
          onTap: (index) {
            setState(() {
              _index = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/icon/homeIcon.svg',
                  color: Colors.black,
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                label: "Accueil",
                /*  title: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Accueil',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        ), */
                activeIcon: SvgPicture.asset(
                  'assets/icon/homeIcon_active.svg',
                  color: Colors.orange[600],
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/icon/compass.svg',
                  color: Colors.black,
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                label: 'Explore',
                /*  title: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Explore',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        ), */
                activeIcon: SvgPicture.asset(
                  'assets/icon/compass.svg',
                  color: Colors.orange[600],
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.bookmark,
                  color: Colors.black,
                  // size: 32,
                ),
                label: 'Enregistre',
                /*    title: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Enregistre',
                            style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
                          ),
                        ), */
                activeIcon: Icon(
                  Icons.bookmark,
                  color: Colors.orange[600],
                  // size: 32,
                ),
                backgroundColor: Colors.white),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  'assets/icon/user.svg',
                  color: Colors.black,
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                label: 'Profile',
                /*  title: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Profile',
                        style: TextStyle(
                            color: Colors.blueGrey[200], fontSize: 11),
                      ),
                    ), */
                activeIcon: SvgPicture.asset(
                  'assets/icon/user.svg',
                  color: Colors.orange[600],
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                backgroundColor: Colors.white),
          ],
        ),
        body: Container(
          color: Colors.grey[200],
          child: SafeArea(
            minimum: EdgeInsets.only(top: 90),
            top: true,
            child: Scaffold(
              backgroundColor: AppTheme.nearlyWhite,
              body: SingleChildScrollView(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: MultiBlocProvider(
                    providers: [
                      BlocProvider<CategoryBloc>.value(
                        value: _blocCategory,
                      ),
                      BlocProvider<PlaceBloc>.value(
                        value: _blocPlace,
                      ),
                      BlocProvider<EvenementBloc>.value(
                        value: _evenementBloc,
                      ),
                    ],
                    child: _widgetList[_index],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
