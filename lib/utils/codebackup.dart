

// ListView.builder(
//               scrollDirection: Axis.horizontal,
//               primary: false,
//               itemCount: places2 == null ? 0 : places2.length,
//               itemBuilder: (BuildContext context, int index) {
//                 Map place = places2.reversed.toList()[index];
//                 return Padding(
//                   padding: const EdgeInsets.only(right: 20),
//                   child: InkWell(
//                     child: Container(
//                       height: 250,
//                       width: 270,
// //                      color: Colors.green,
//                       child: Column(
//                         children: <Widget>[
//                           ClipRRect(
//                             borderRadius: BorderRadius.circular(10),
//                             child: Image.asset(
//                               "${place["img"]}",
//                               height: 178,
//                               width: 260,
//                               fit: BoxFit.cover,
//                             ),
//                           ),
//                           SizedBox(height: 7),
//                           Container(
//                             alignment: Alignment.centerLeft,
//                             child: Text(
//                               "${place["name"]}",
//                               style: TextStyle(
//                                 fontWeight: FontWeight.bold,
//                                 fontSize: 15,
//                               ),
//                               maxLines: 2,
//                               textAlign: TextAlign.left,
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                     onTap: () {
//                       Navigator.of(context).push(
//                         MaterialPageRoute(
//                           builder: (BuildContext context) {
//                             return Details();
//                           },
//                         ),
//                       );
//                     },
//                   ),
//                 );
//               },
//             ),

// Positioned(
//               top: (MediaQuery.of(context).size.width / 1.2) - 24.0 - 35,
//               right: 35,
//               child: ScaleTransition(
//                 alignment: Alignment.center,
//                 scale: CurvedAnimation(
//                     parent: animationController, curve: Curves.fastOutSlowIn),
//                 child: Card(
//                   color: AppTheme.nearlyBlue,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(50.0)),
//                   elevation: 10.0,
//                   child: Container(
//                     width: 60,
//                     height: 60,
//                     child: Center(
//                       child: Icon(
//                         Icons.favorite,
//                         color: AppTheme.nearlyWhite,
//                         size: 30,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),

// import 'package:flutter/material.dart';
// import 'package:wineapp/pages/explore.dart';
// import 'package:wineapp/pages/home.dart';
// import 'package:wineapp/pages/home_list.dart';
// import 'package:wineapp/pages/home_list_page.dart';
// import 'package:wineapp/pages/profile_one_page.dart';
// import 'package:flutter_svg/flutter_svg.dart';


// class HomeNewPage extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _HomeNewPageState();
//   }
// }

//   class _HomeNewPageState extends State<HomeNewPage> {


//   int _index = 0;
  
//   static void moveTo() {
//     // Navigator.push<dynamic>(
//     //   context,
//     //   MaterialPageRoute<dynamic>(
//     //     builder: (BuildContext context) => CourseInfoScreen(),
//     //   ),
//     // );
//   }


// List<Widget> _widgetList = [
//     Home(
//       callBack: () {
//         moveTo();
//         },
//     ),
//     Explore(
//       callBack: () {
//         moveTo();
//         },
//     ),
//     HomeList(
//       callBack: () {
//         moveTo();
//         },
//     ),
//     ProfileOnePage(),
//   ];
  


//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       // color: Colors.black,
//       child: Scaffold(
//         backgroundColor: Colors.white,
//         appBar: AppBar(
//           leading: Image.asset('assets/icon/ic_launcher.png'),
//           elevation: 0,
//           backgroundColor: Colors.transparent,
//           brightness: Brightness.light,
//           actions: <Widget>[
//             Container(
//               padding: EdgeInsets.only(top: 10, bottom: 10, left: 32, right: 32),
//               height: 80,
//               child: Container(
//                 height: 80,
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(45),
//                   //color: Color(0x50F02D2D),
//                   color: Colors.white
//                 ),
//                 child: Row(
//                   children: <Widget>[
//                     Padding(
//                       padding: const EdgeInsets.only(left: 8, right: 16, top: 10),
//                       child: Icon(
//                         Icons.menu,
//                         color: Colors.black,
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             )
//           ],
//         ),

//         bottomNavigationBar: BottomNavigationBar(
//           showSelectedLabels: true,
//           showUnselectedLabels: true,
//           selectedItemColor: Colors.green,
//         unselectedItemColor: Colors.black,
//         type: BottomNavigationBarType.shifting,
//         currentIndex: _index,
//         onTap: (index) {
//           setState(() {
//             _index = index;
//           });
//         },
//           items: [
//             BottomNavigationBarItem(
//               icon: Icon(
//                           Icons.home,
//                           color: Colors.black,
//                           // size: 32,
//                         ),
//               title: Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Text(
//                             'Accueil',
//                             style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
//                           ),
//                         ),
//               activeIcon: Icon(
//                           Icons.home,
//                           color: Colors.orange[600],
//                           // size: 32,
//                         ),
//               backgroundColor: Colors.white
//               ),
//               BottomNavigationBarItem(
//               icon: SvgPicture.asset(
//                 'assets/icon/compass.svg',
//                 color: Colors.red,
//                 allowDrawingOutsideViewBox: false,
//                 alignment: Alignment.center,
//                 height: 25,
//                 width: 25,
//                 ),
//               title: Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Text(
//                             'Explore',
//                             style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
//                           ),
//                         ),
//               activeIcon: Icon(
//                           Icons.explore,
//                           color: Colors.orange[600],
//                           // size: 32,
//                         ),
//               backgroundColor: Colors.white
//               ),
//               BottomNavigationBarItem(
//               icon: Icon(
//                           Icons.bookmark,
//                           color: Colors.black,
//                           // size: 32,
//                         ),
//               title: Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Text(
//                             'Enregistre',
//                             style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
//                           ),
//                         ),
//               activeIcon: Icon(
//                           Icons.bookmark,
//                           color: Colors.orange[600],
//                           // size: 32,
//                         ),
//               backgroundColor: Colors.white
//               ),
//               BottomNavigationBarItem(
//               icon: Icon(
//                           Icons.person,
//                           color: Colors.black,
//                           // size: 32,
//                         ),
//               title: Padding(
//                           padding: const EdgeInsets.all(8.0),
//                           child: Text(
//                             'Profile',
//                             style: TextStyle(color: Colors.blueGrey[200], fontSize: 11),
//                           ),
//                         ),
//               activeIcon: Icon(
//                           Icons.person,
//                           color: Colors.orange[600],
//                           // size: 32,
//                         ),
//               backgroundColor: Colors.white
//               ),
              
//           ],
//       ),
//       body: _widgetList[_index]
//       )
//     );
//   }
// }