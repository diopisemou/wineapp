import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/place/place_bloc.dart';
import 'package:wineapp/models/place.dart';
import 'package:wineapp/pages/details.dart';
import 'package:shimmer/shimmer.dart';

class CarouselPlace extends StatefulWidget {
  const CarouselPlace({Key key}) : super(key: key);

  @override
  _CarouselPlaceState createState() => _CarouselPlaceState();
}

class _CarouselPlaceState extends State<CarouselPlace> {
  @override
  void initState() {
    super.initState();
    context.read<PlaceBloc>().add(LoadPlaces());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, left: 20),
      height: 250,
      width: MediaQuery.of(context).size.width,
      child: BlocBuilder<PlaceBloc, PlaceState>(
        builder: (context, state) {
          if (state is PlaceLoaded) {
            final places = state.places;
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              primary: false,
              itemCount: places.isEmpty ? 0 : places.length,
              itemBuilder: (_, index) => CarouselItemPlace(
                place: places.elementAt(index),
              ),
            );
          }
          return ListView.builder(
            scrollDirection: Axis.horizontal,
            primary: false,
            itemCount: 3,
            itemBuilder: (__, index) => Shimmer.fromColors(
              child: CarouselItemPlace(place: null),
              baseColor: Colors.white10,
              highlightColor: Colors.white70,
            ),
          );
        },
      ),
    );
  }
}

class CarouselItemPlace extends StatelessWidget {
  const CarouselItemPlace({
    Key key,
    @required this.place,
  }) : super(key: key);

  final Place place;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: InkWell(
        child: Container(
          height: 250,
          width: 270,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                /** Prendre au hasard une image */
                child: place == null
                    ? Container(
                        width: 260,
                        height: 178,
                        color: Colors.grey,
                      )
                    : Image.asset(
                        "${place.img.elementAt(Random().nextInt(place.img.length))}",
                        width: 260,
                        height: 178,
                        fit: BoxFit.cover,
                      ),
              ),
              SizedBox(height: 7),
              Container(
                alignment: Alignment.centerLeft,
                child: place == null
                    ? Container(
                        width: 260,
                        height: 8.0,
                        color: Colors.grey,
                      )
                    : Text(
                        "${place.name}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                        maxLines: 2,
                        textAlign: TextAlign.left,
                      ),
              ),
              SizedBox(height: 3),
              Container(
                alignment: Alignment.centerLeft,
                child: place == null
                    ? Container(
                        width: 260,
                        height: 8.0,
                        color: Colors.grey,
                      )
                    : Text(
                        "${place.location}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                          color: Colors.blueGrey[300],
                        ),
                        maxLines: 1,
                        textAlign: TextAlign.left,
                      ),
              ),
            ],
          ),
        ),
        onTap: place == null
            ? null
            : () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (BuildContext context) {
                      return Details(place: place);
                    },
                  ),
                );
              },
      ),
    );
  }
}
