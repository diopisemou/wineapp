import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/category/category_bloc.dart';
import 'package:wineapp/bloc/place/place_bloc.dart';
import 'package:wineapp/pages/place_info_screen.dart';
import 'package:wineapp/pages/popular_categories_list_view.dart';
import 'package:wineapp/utils/app_theme.dart';
import 'package:wineapp/widgets/carousel_evenement.dart';
import 'package:wineapp/widgets/carousel_place.dart';

class HomeTab extends StatefulWidget {
  const HomeTab({Key key, this.goToEnregister}) : super(key: key);

  final Function goToEnregister;
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> with TickerProviderStateMixin {
  final TextEditingController _searchControl = new TextEditingController();
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
    super.initState();
  }

  void moveTo() {
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => PlaceInfoScreen(),
      ),
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void didChangeDependencies() {
    context.read<CategoryBloc>().add(LoadCategories());
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        body: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      CarouselEvenement(),
                      Expanded(child: getCategoriesUI(context)),
                      /* Flexible(
                        fit: FlexFit.loose,
                        child: getCategoriesUI(context),
                      ), */
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getCategoriesUI(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 18, right: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Categories ',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 22,
              letterSpacing: 0.27,
              color: AppTheme.darkerText,
            ),
          ),
          Expanded(
            child: BlocBuilder<CategoryBloc, CategoryState>(
              buildWhen: (prState, nextState) => nextState is CategoryLoaded,
              builder: (context, state) {
                if (state is CategoryLoaded)
                  return PopularCategoriesListView(
                    categories: state.categories,
                    callBack: (keyCategory) {
                      context
                          .read<PlaceBloc>()
                          .add(LoadPlacesForCategory(keyCategory));
                      widget.goToEnregister();
                      // moveTo();
                    },
                  );
                return Container();
              },
            ),
          )
        ],
      ),
    );
  }
}
