part of 'category_bloc.dart';

abstract class CategorieEvent {}

class LoadCategories extends CategorieEvent {
  @override
  String toString() => "LoadCatetory";
}

class LoadFavoriteCategories extends CategorieEvent {
  @override
  String toString() => "LoadFavoriteCategories";
}

class LoadCategoriesForChoice extends CategorieEvent {
  @override
  String toString() => "LoadCategoriesForChoice";
}

class AddCategory extends CategorieEvent {
  final Category category;

  AddCategory({this.category});
}

class DeleteFavoriteCategory extends CategorieEvent {
  final Category category;

  DeleteFavoriteCategory(this.category);
}

class DeleteFavoriteCategories extends CategorieEvent {
  final List<Category> categories;

  DeleteFavoriteCategories(this.categories);
}

class CategoriesUpdated extends CategorieEvent {
  final List<Category> categories;

  CategoriesUpdated(this.categories);
}

class FavoriteCategoriesUpdated extends CategorieEvent {
  final List<Category> categories;

  FavoriteCategoriesUpdated(this.categories);
  @override
  String toString() => "FavoriteCategoriesUpdated";
}

class UpdateFavoriteCategories extends CategorieEvent {
  final List<Category> categories;

  UpdateFavoriteCategories(this.categories);
  @override
  String toString() => "UpdateFavoriteCategories";
}

class ToogleFavorite extends CategorieEvent {
  final List<CategoryForChooseFavorite> categoriesForChoose;
  final CategoryForChooseFavorite categoryForChooseFavorite;

  ToogleFavorite({
    this.categoryForChooseFavorite,
    this.categoriesForChoose,
  });
}

class LongPressFavoriteCategory extends CategorieEvent {
  final List<CategoryForChooseFavorite> favoritecategoriesForChoice;
  final CategoryForChooseFavorite pressedFavorite;

  LongPressFavoriteCategory({
    this.favoritecategoriesForChoice,
    this.pressedFavorite,
  });
}
