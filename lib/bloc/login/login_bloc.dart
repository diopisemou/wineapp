import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:wineapp/bloc/authentifiation/authentification_bloc.dart';
import 'package:wineapp/services/authentication.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final BaseAuth _baseAuth;
  final AuthentificationBloc _authentificationBloc;
  LoginBloc({
    @required BaseAuth baseAuth,
    @required AuthentificationBloc authentificationBloc,
  })  : assert(baseAuth != null && authentificationBloc != null),
        _baseAuth = baseAuth,
        _authentificationBloc = authentificationBloc,
        super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginButtonPressed) {
      yield* _mapLoginButtonPressedToState(event);
    } else if (event is LogoutButtonPresses) {
      yield* mapLogoutButtonPressesToState(event);
    }
  }

  Stream<LoginState> _mapLoginButtonPressedToState(LoginButtonPressed event) {}

  Stream<LoginState> mapLogoutButtonPressesToState(LogoutButtonPresses event) {}
}
