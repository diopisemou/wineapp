import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:wineapp/models/place.dart';
import 'package:wineapp/services/place_service.dart';

part 'place_event.dart';
part 'place_state.dart';

class PlaceBloc extends Bloc<PlaceEvent, PlaceState> {
  final PlaceService _placeService;
  PlaceBloc({@required PlaceService placeService})
      : assert(placeService != null),
        _placeService = placeService,
        super(PlaceInitial());

  @override
  Stream<PlaceState> mapEventToState(
    PlaceEvent event,
  ) async* {
    if (event is LoadPlaces) {
      yield* _mapLoadPlacesToState(event);
    } else if (event is LoadPlacesForCategory) {
      yield* _mapLoadPlacesForCategoryToState(event);
    }
  }

  Stream<PlaceState> _mapLoadPlacesToState(LoadPlaces event) async* {
    final places = await _placeService.places();
    yield PlaceLoaded(places);
  }

  Stream<PlaceState> _mapLoadPlacesForCategoryToState(
      LoadPlacesForCategory event) async* {
    final places = await _placeService.findPlacesByCategory(event.keyCategory);
    yield PlaceForCategoryLoaded(places);
  }

  Future<List<Place>> getPlaceSuggestion(String pattern) async {
    return await _placeService.getSuggestions(pattern);
  }
}
