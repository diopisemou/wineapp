import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wineapp/bloc/profil/profil_bloc.dart';

class FormPasseWord extends StatefulWidget {
  final Function(String newPasseWord) onSave;
  FormPasseWord({Key key, @required this.onSave}) : super(key: key);

  @override
  _CustomFormEditPassState createState() => _CustomFormEditPassState();
}

class _CustomFormEditPassState extends State<FormPasseWord> {
  final _formKey = GlobalKey<FormState>();
  String oldPassword;
  String newPassword;
  String comfirmedPassword;
  @override
  void initState() {
    super.initState();
  }

  inputDecoration(String label) {
    return InputDecoration(
      filled: true,
      fillColor: Colors.grey[200],
      labelText: label,
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: BlocBuilder<ProfilBloc, ProfilState>(builder: (context, state) {
        if (state is ProfilPasswordState)
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: TextFormField(
                  style: TextStyle(color: Colors.black, fontSize: 20),
                  obscureText: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (_) {
                    return state.isIncorrectPassword == null
                        ? null
                        : state.isIncorrectPassword
                            ? 'Invalid Password'
                            : null;
                  },
                  decoration: inputDecoration("Saisir l'ancien mot de passe"),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: TextFormField(
                  style: TextStyle(color: Colors.black, fontSize: 20),
                  obscureText: true,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  onChanged: (val) => newPassword = val,
                  validator: (value) {
                    if (value == null || value.isEmpty)
                      return "mot de passe invalide";
                    return null;
                  },
                  decoration:
                      inputDecoration("Saisir votre nouveau mot de passe"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: TextFormField(
                  style: TextStyle(color: Colors.black, fontSize: 20),
                  obscureText: true,
                  onChanged: (val) => comfirmedPassword = val,
                  validator: (value) {
                    if (comfirmedPassword != newPassword)
                      return 'les mots de passe ne correspondent pas';
                    return null;
                  },
                  decoration:
                      inputDecoration("Confirmer le nouveau mot de passe"),
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  color: Colors.orangeAccent[700],
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      context.read<ProfilBloc>().add(UpdatePassword(
                            oldPassword: oldPassword,
                            newPassword: newPassword,
                          ));
                    }
                  },
                  child: Text(
                    "Enregister",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ),
              ),
            ],
          );
        return CircularProgressIndicator();
      }),
    );
  }
}

class SuccessDialog extends StatelessWidget {
  final VoidCallback onDismissed;

  SuccessDialog({Key key, @required this.onDismissed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Icon(Icons.info),
              Flexible(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Form Submitted Successfully!',
                    softWrap: true,
                  ),
                ),
              ),
            ],
          ),
          RaisedButton(
            child: Text('OK'),
            onPressed: onDismissed,
          ),
        ],
      ),
    );
  }
}
