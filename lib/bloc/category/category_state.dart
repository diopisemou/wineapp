part of 'category_bloc.dart';

@immutable
abstract class CategoryState extends Equatable {
  List<Object> get props => [];
}

class CategorieInitial extends CategoryState {
  @override
  String toString() => "CategorieInitial";
}

class CategoryLoaded extends CategoryState {
  final List<Category> categories;

  CategoryLoaded(this.categories);
  List<Object> get props => [categories];
  @override
  String toString() => "CategoryLoaded";
}

class CategoryLoadedForChoiceFavorite extends CategoryState {
  final List<CategoryForChooseFavorite> categoriesForChooseFavorite;

  CategoryLoadedForChoiceFavorite(this.categoriesForChooseFavorite);
  List<Object> get props => [categoriesForChooseFavorite];

  List<Category> get categories =>
      categoriesForChooseFavorite.map((e) => e.category).toList();
  List<CategoryForChooseFavorite> get favoriteCategoriesForChoice =>
      categoriesForChooseFavorite
          .where((element) => element.isFavorite)
          //.map((e) => e.category)
          .toList();
  List<Category> get favoriteCategories => categoriesForChooseFavorite
      .where((element) => element.isFavorite)
      .map((e) => e.category)
      .toList();

  @override
  String toString() => "CategoryLoadedForChooseFavorite";
}

class FavoriteCategoriesLoaded extends CategoryState {
  final List<CategoryForChooseFavorite> favoritecategoriesForChoice;

  FavoriteCategoriesLoaded(this.favoritecategoriesForChoice);

  List<Object> get props => [favoritecategoriesForChoice];
  int get favoriteToRemoveCount => favoritecategoriesForChoice
      .where((item) => !item.isFavorite)
      .length;
  @override
  String toString() => "FavoriteCategoriesLoaded";
}

class AfterFavoriteCategoriesUpdated extends CategoryState {
   final UpdateStatus updateStatus;
   final String message;

  AfterFavoriteCategoriesUpdated({this.updateStatus, this.message});
  @override
  String toString() => "AfterFavoriteCategoriesUpdated";
}



class CategoryForChooseFavorite {
  final Category category;
  final bool isFavorite;

  CategoryForChooseFavorite({this.category, this.isFavorite = true});

  CategoryForChooseFavorite copyWith({
    Category category,
    bool isFavorite,
  }) {
    return CategoryForChooseFavorite(
      category: category ?? this.category,
      isFavorite: isFavorite ?? this.isFavorite,
    );
  }
}
