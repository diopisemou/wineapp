import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wineapp/models/place.dart';
import 'package:wineapp/pages/menu_details.dart';
import 'package:wineapp/utils/app_theme.dart';

class PlaceInfoScreen extends StatefulWidget {
  final Place place;

  const PlaceInfoScreen({Key key, @required this.place}) : super(key: key);
  @override
  _PlaceInfoScreenState createState() => _PlaceInfoScreenState();
}

class _PlaceInfoScreenState extends State<PlaceInfoScreen>
    with TickerProviderStateMixin {
  final double infoHeight = 600.0;
  AnimationController animationController;
  Animation<double> animation;
  double opacity1 = 0.0;
  double opacity2 = 0.0;
  double opacity3 = 0.0;
  double _leftPadding = 0;
  @override
  void initState() {
    animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 1000));
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(0, 1.0, curve: Curves.fastOutSlowIn)));
    setData();

    super.initState();
  }

  Future<void> setData() async {
    animationController.forward();
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity1 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity2 = 1.0;
    });
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    setState(() {
      opacity3 = 1.0;
    });
  }

  void openMenuImage() {
    Navigator.push<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => MenuDetails(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double tempHeight = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.width / 1.2) +
        200.0;
    return Container(
        color: AppTheme.nearlyWhite, child: getBodyUI2(tempHeight));
  }

  Widget getBodyUI2(double tempHeight) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              pinned: true,
              floating: false,
              snap: false,
              expandedHeight: 425.0,
              stretch: true,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: false,
                titlePadding: EdgeInsets.all(0),
                collapseMode: CollapseMode.none,
                title: HeaderBoxUIWidget(place: widget.place),
                background: AspectRatio(
                  aspectRatio: 1.2,
                  child: Image.asset(
                    'assets/images/webInterFace.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ];
        },
        body: Container(
          decoration: BoxDecoration(
            color: AppTheme.nearlyGrey,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(32.0),
                topRight: Radius.circular(32.0)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: AppTheme.grey.withOpacity(0.2),
                  offset: const Offset(1.1, 1.1),
                  blurRadius: 10.0),
            ],
          ),
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: ListView(
            children: <Widget>[
              getDescriptionBoxUI(),
              getMenuBoxUI(),
              SizedBox(height: 10),
              getRecommendBoxUI(),
              getRatingBoxUI(),
              getFeatureHeaderBoxUI(),
              getFeaturesBoxUI(),
              SizedBox(height: 100)
            ],
          ),
        ),
        /*   slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: AppTheme.nearlyGrey,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(32.0),
                        topRight: Radius.circular(32.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppTheme.grey.withOpacity(0.2),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        constraints: BoxConstraints(
                            minHeight: infoHeight,
                            maxHeight: tempHeight > infoHeight
                                ? tempHeight
                                : infoHeight),
                        child: ListView(
                          /*  mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start, */
                          children: <Widget>[
                            getDescriptionBoxUI(),
                            getMenuBoxUI(),
                            SizedBox(
                              height: 10,
                            ),
                            getRecommendBoxUI(),
                            getRatingBoxUI(),
                            getFeatureHeaderBoxUI(),
                            getFeaturesBoxUI(),
                            SizedBox(
                              height: MediaQuery.of(context).padding.bottom,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      */
      ),
      floatingActionButton: getContactBoxUI(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget getBodyUI(double tempHeight) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            floating: false,
            snap: false,
            expandedHeight: 425.0,
            stretch: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: false,
              titlePadding: EdgeInsets.all(0),
              title: ScaleTransition(
                  scale: CurvedAnimation(
                      parent: animationController, curve: Curves.fastOutSlowIn),
                  child: HeaderBoxUIWidget(place: widget.place)),
              background: AspectRatio(
                aspectRatio: 1.2,
                child: Image.asset(
                  'assets/images/webInterFace.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: AppTheme.nearlyGrey,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(32.0),
                        topRight: Radius.circular(32.0)),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppTheme.grey.withOpacity(0.2),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8, right: 8),
                    child: SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        constraints: BoxConstraints(
                            minHeight: infoHeight,
                            maxHeight: tempHeight > infoHeight
                                ? tempHeight
                                : infoHeight),
                        child: ListView(
                          /*  mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start, */
                          children: <Widget>[
                            getDescriptionBoxUI(),
                            getMenuBoxUI(),
                            SizedBox(height: 10),
                            getRecommendBoxUI(),
                            getRatingBoxUI(),
                            getFeatureHeaderBoxUI(),
                            getFeaturesBoxUI(),
                            SizedBox(
                              height: MediaQuery.of(context).padding.bottom,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      floatingActionButton: getContactBoxUI(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget getHeaderBoxUI() {
    // return HeaderBoxUIWidget(pa);
  }

  Widget getDescriptionBoxUI() {
    return AnimatedOpacity(
      duration: const Duration(milliseconds: 500),
      opacity: opacity2,
      child: Container(
        margin: const EdgeInsets.only(top: 10, bottom: 8),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  'Restaurant Bar situé à Glass (Libreville, Gabon)',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 15,
                      letterSpacing: 0.27,
                      color: AppTheme.darkText,
                      fontFamily: 'Railway Medium'),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  'Réservations : banturestaurantgabon.com',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 15,
                      letterSpacing: 0.27,
                      color: AppTheme.darkerText,
                      fontFamily: 'Railway Medium'),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  'Téléphone :',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 15,
                    letterSpacing: 0.27,
                    color: AppTheme.darkText,
                  ),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  ' +241 74 14 49 49',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 15,
                      letterSpacing: 0.27,
                      color: Colors.orange[300],
                      fontFamily: 'Railway Medium'),
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getMenuBoxUI() {
    return Container(
      margin: const EdgeInsets.only(top: 35, bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Menu',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 22,
                letterSpacing: 0.27,
                color: AppTheme.dark_grey,
                fontFamily: 'Railway Bold'),
          ),
          getMenuBoxImagesUI(),
        ],
      ),
    );
  }

  Widget getMenuBoxImagesUI() {
    return Container(
      margin: const EdgeInsets.only(left: 6, right: 6),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getImageThumbnailUI(
              "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
          getImageThumbnailUI(
              "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
          getImageThumbnailUI(
              "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
          getImageThumbnailUI(
              "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
          getImageThumbnailUI(
              "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
        ],
      ),
    );
  }

  Widget getImageThumbnailUI(String imagepath) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: () {
        openMenuImage();
      },
      child: Container(
        height: 45,
        width: 45,
        margin: const EdgeInsets.only(left: 6),
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.only(
            topLeft: new Radius.circular(5.0),
            topRight: new Radius.circular(5.0),
            bottomLeft: new Radius.circular(5.0),
            bottomRight: new Radius.circular(5.0),
          ),
          image: new DecorationImage(
            image: NetworkImage(imagepath),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Widget getRecommendBoxUI() {
    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 10),
        onPressed: () {},
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset(
              'assets/images/favoriteplace.svg',
              color: Colors.orange[300],
              allowDrawingOutsideViewBox: false,
              alignment: Alignment.center,
              height: 25,
              width: 25,
            ),
            SizedBox(width: 20),
            Text(
              'Recommander ce lieu ',
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                  letterSpacing: 0.27,
                  color: AppTheme.dark_grey,
                  fontFamily: 'Railway Bold'),
            ),
          ],
        ),
      ),
    );
  }

  Widget getRatingBoxUI() {
    return Container(
      margin: const EdgeInsets.only(top: 35, bottom: 8),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8, top: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Note :',
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 22,
                  letterSpacing: 0.27,
                  color: AppTheme.dark_grey,
                  fontFamily: 'Railway Bold'),
            ),
            Container(
              margin: const EdgeInsets.only(left: 6, right: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.star,
                    color: Colors.orange[300],
                    size: 22,
                  ),
                  Icon(
                    Icons.star,
                    color: Colors.orange[300],
                    size: 22,
                  ),
                  Icon(
                    Icons.star,
                    color: Colors.orange[300],
                    size: 22,
                  ),
                  Icon(
                    Icons.star,
                    color: Colors.orange[300],
                    size: 22,
                  ),
                  Icon(
                    Icons.star,
                    color: Colors.orange[300],
                    size: 22,
                  ),
                  Icon(
                    Icons.star,
                    color: AppTheme.grey,
                    size: 22,
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 0, right: 6),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(
                        "https://avatars0.githubusercontent.com/u/12619420?s=460&v=4"),
                    foregroundColor: Colors.black,
                    radius: 15.0,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 16, right: 6, top: 6),
                    width: 20,
                    height: 20,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.green[300],
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                        border:
                            Border.all(color: AppTheme.grey.withOpacity(0.2)),
                      ),
                      child: Icon(
                        Icons.thumb_up,
                        color: Colors.white,
                        size: 15,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 6, right: 6, top: 6),
                    width: 20,
                    height: 20,
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppTheme.grey,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                        border:
                            Border.all(color: AppTheme.grey.withOpacity(0.2)),
                      ),
                      child: Icon(
                        Icons.thumb_down,
                        color: AppTheme.white,
                        size: 15,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getFeatureHeaderBoxUI() {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 8),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Text(
              "Conditions de l'espace",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 25,
                  letterSpacing: 0.27,
                  color: Colors.deepOrange[200],
                  fontFamily: 'Railway Bold'),
            ),
            SizedBox(height: 5),
            Text(
              "Retrouvez les différents services additionnels",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  letterSpacing: 0.27,
                  color: Colors.black,
                  fontFamily: 'Railway Bold'),
            )
          ],
        ),
      ),
    );
  }

  Widget getFeatureBoxUI(String featureText, Widget iconData,
      {bool isPresent = true}) {
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5)),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 20),
            iconData,
            SizedBox(width: 8),
            Expanded(
              child: Text(
                featureText,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    letterSpacing: 0.3,
                    color: AppTheme.dark_grey,
                    fontFamily: 'Railway Bold'),
              ),
            ),
            SizedBox(width: 30),
            Icon(
              Icons.circle,
              color: isPresent ? Colors.green[300] : Colors.red,
              size: 25,
            ),
            SizedBox(width: 20),
          ],
        ),
      ),
    );
  }

  Widget getFeaturesBoxUI() {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      child: Padding(
        padding: EdgeInsets.only(left: 6, right: 6, bottom: 0, top: 0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            getFeatureBoxUI(
                "Térasse",
                SvgPicture.asset(
                  'assets/images/terasse.svg',
                  color: Colors.orange[300],
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                isPresent: widget.place.hasTerasse),
            getFeatureBoxUI(
              "WI-FI",
              SvgPicture.asset(
                'assets/images/wifilogo.svg',
                color: Colors.orange[300],
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
                height: 25,
                width: 25,
              ),
              isPresent: widget.place.hasWifi,
            ),
            getFeatureBoxUI(
                "Parking",
                SvgPicture.asset(
                  'assets/images/parkinglogo.svg',
                  color: Colors.orange[300],
                  allowDrawingOutsideViewBox: false,
                  alignment: Alignment.center,
                  height: 25,
                  width: 25,
                ),
                isPresent: widget.place.hasParking),
            getFeatureBoxUI(
              "Réservation",
              SvgPicture.asset(
                'assets/images/resalogo.svg',
                color: Colors.orange[300],
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
                height: 25,
                width: 25,
              ),
              isPresent: widget.place.hasResevation,
            ),
            getFeatureBoxUI(
              "Fumeur",
              SvgPicture.asset(
                'assets/images/smokelogo.svg',
                color: Colors.orange[300],
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
                height: 25,
                width: 25,
              ),
              isPresent: widget.place.hasFumeur,
            ),
          ],
        ),
      ),
    );
  }

  Widget getContactBoxUI() {
    return AnimatedOpacity(
      duration: const Duration(milliseconds: 500),
      opacity: opacity3,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        onPressed: () {},
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xffF5D020), Color(0xffF53803)],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.orange[600].withOpacity(0.5),
                offset: Offset(1.1, 1.1),
                blurRadius: 10.0,
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Contacter',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    letterSpacing: 0.0,
                    color: AppTheme.nearlyWhite,
                  ),
                ),
                SizedBox(width: 10),
                Icon(
                  Icons.phone_in_talk,
                  color: AppTheme.white,
                  size: 28,
                ),
              ],
            ),
          ),
        ),
      ),
    );
    return Container(
      color: Colors.transparent,
      width: MediaQuery.of(context).size.width,
      height: 80,
      margin: const EdgeInsets.only(top: 5, bottom: 0),
      child: AnimatedOpacity(
        duration: const Duration(milliseconds: 500),
        opacity: opacity3,
        child: Padding(
          padding: const EdgeInsets.only(left: 6, bottom: 16, right: 6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(width: 90),
              Expanded(
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.orange[600], //AppTheme.nearlyBlue,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(8.0),
                      ),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.orange[600].withOpacity(0.5),
                            offset: const Offset(1.1, 1.1),
                            blurRadius: 10.0),
                      ],
                    ),
                    child: Center(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Contacter',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              letterSpacing: 0.0,
                              color: AppTheme.nearlyWhite,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.phone_in_talk,
                            color: AppTheme.white,
                            size: 28,
                          ),
                          SizedBox(
                            width: 10,
                          )
                        ])),
                  ),
                ),
              ),
              SizedBox(width: 90),
            ],
          ),
        ),
      ),
    );
  }

  Widget getAddEventBoxUI() {
    return Container(
      margin: const EdgeInsets.only(top: 25, bottom: 0),
      child: AnimatedOpacity(
        duration: const Duration(milliseconds: 500),
        opacity: opacity3,
        child: Padding(
          padding: const EdgeInsets.only(left: 16, bottom: 16, right: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 48,
                height: 48,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppTheme.nearlyWhite,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    border: Border.all(color: AppTheme.grey.withOpacity(0.2)),
                  ),
                  child: Icon(
                    Icons.add,
                    color: AppTheme.nearlyBlue,
                    size: 28,
                  ),
                ),
              ),
              SizedBox(width: 16),
              Expanded(
                child: Container(
                  height: 48,
                  decoration: BoxDecoration(
                    color: AppTheme.nearlyBlue,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(16.0),
                    ),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppTheme.nearlyBlue.withOpacity(0.5),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      'Join Course',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        letterSpacing: 0.0,
                        color: AppTheme.nearlyWhite,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getTimeBoxUI(String text1, String txt2) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          color: AppTheme.nearlyWhite,
          borderRadius: const BorderRadius.all(Radius.circular(16.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: AppTheme.grey.withOpacity(0.2),
                offset: const Offset(1.1, 1.1),
                blurRadius: 8.0),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              left: 18.0, right: 18.0, top: 12.0, bottom: 12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                text1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: AppTheme.nearlyBlue,
                ),
              ),
              Text(
                txt2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w200,
                  fontSize: 14,
                  letterSpacing: 0.27,
                  color: AppTheme.grey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class HeaderBoxUIWidget extends StatefulWidget {
  final Place place;
  const HeaderBoxUIWidget({
    Key key,
    @required this.place,
  }) : super(key: key);

  @override
  _HeaderBoxUIWidgetState createState() => _HeaderBoxUIWidgetState();
}

class _HeaderBoxUIWidgetState extends State<HeaderBoxUIWidget> {
  ScrollPosition _position;
  double _leftPadding = 0;

  /// default padding
  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    _removeListener();
    _addListener();
    super.didChangeDependencies();
  }

  void _addListener() {
    _position = Scrollable.of(context)?.position;
    _position?.addListener(_positionListener);
    _positionListener();
  }

  void _removeListener() => _position?.removeListener(_positionListener);

  void _positionListener() {
    /// when scroll position changes widget will be rebuilt
    final FlexibleSpaceBarSettings settings =
        context.dependOnInheritedWidgetOfExactType();
    setState(() => _leftPadding = getPadding(settings.minExtent.toInt(),
        settings.maxExtent.toInt(), settings.currentExtent.toInt()));
  }

  double getPadding(int minExtent, int maxExtent, int currentExtent) {
    double onePaddingExtent = (maxExtent - minExtent) / 17;

    /// onePaddingExtent stands for 1 logical pixel of padding, 17 = count of numbers in range from 0 to 16
    /// when currentExtent changes the padding smoothly change
    if (currentExtent >= minExtent &&
        currentExtent <= minExtent + (1 * onePaddingExtent))
      return 20;
    else if (currentExtent > minExtent + (1 * onePaddingExtent) &&
        currentExtent <= minExtent + (2 * onePaddingExtent))
      return 15;
    else if (currentExtent > minExtent + (2 * onePaddingExtent) &&
        currentExtent <= minExtent + (3 * onePaddingExtent))
      return 14;
    else if (currentExtent > minExtent + (3 * onePaddingExtent) &&
        currentExtent <= minExtent + (4 * onePaddingExtent))
      return 13;
    else if (currentExtent > minExtent + (4 * onePaddingExtent) &&
        currentExtent <= minExtent + (5 * onePaddingExtent))
      return 12;
    else if (currentExtent > minExtent + (5 * onePaddingExtent) &&
        currentExtent <= minExtent + (6 * onePaddingExtent))
      return 11;
    else if (currentExtent > minExtent + (6 * onePaddingExtent) &&
        currentExtent <= minExtent + (7 * onePaddingExtent))
      return 10;
    else if (currentExtent > minExtent + (7 * onePaddingExtent) &&
        currentExtent <= minExtent + (8 * onePaddingExtent))
      return 9;
    else if (currentExtent > minExtent + (8 * onePaddingExtent) &&
        currentExtent <= minExtent + (9 * onePaddingExtent))
      return 8;
    else if (currentExtent > minExtent + (9 * onePaddingExtent) &&
        currentExtent <= minExtent + (10 * onePaddingExtent))
      return 7;
    else if (currentExtent > minExtent + (10 * onePaddingExtent) &&
        currentExtent <= minExtent + (11 * onePaddingExtent))
      return 6;
    else if (currentExtent > minExtent + (11 * onePaddingExtent) &&
        currentExtent <= minExtent + (12 * onePaddingExtent))
      return 5;
    else if (currentExtent > minExtent + (12 * onePaddingExtent) &&
        currentExtent <= minExtent + (13 * onePaddingExtent))
      return 4;
    else if (currentExtent > minExtent + (13 * onePaddingExtent) &&
        currentExtent <= minExtent + (14 * onePaddingExtent))
      return 3;
    else if (currentExtent > minExtent + (14 * onePaddingExtent) &&
        currentExtent <= minExtent + (15 * onePaddingExtent))
      return 2;
    else if (currentExtent > minExtent + (15 * onePaddingExtent) &&
        currentExtent <= minExtent + (16 * onePaddingExtent))
      return 1;
    else if (currentExtent > minExtent + (16 * onePaddingExtent) &&
        currentExtent <= minExtent + (17 * onePaddingExtent))
      return 0;
    else
      return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(0),
      color: Colors.orange[300],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0))),
      elevation: 10.0,
      child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/RectanglePlaceName.png"),
              fit: BoxFit.fill,
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0),
              topRight: Radius.circular(32.0),
            ),
          ),
          height: 53,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: EdgeInsets.only(top: 6.0, left: 20, right: 1),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: 2.0, left: _leftPadding, right: 12),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    widget.place.name,
                                    maxLines: 1,
                                    softWrap: false,
                                    overflow: TextOverflow.fade,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      letterSpacing: 0.27,
                                      color: AppTheme.nearlyWhite,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: _leftPadding, top: 2.0, right: 0),
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 1,
                                  ),
                                  Text(
                                    'Restaurant Bar',
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12,
                                      letterSpacing: 0.27,
                                      color: AppTheme.nearlyWhite,
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      )),
                ),
                Row(
                  children: [
                    Card(
                      color: AppTheme.white,
                      shadowColor: Colors.red,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0)),
                      elevation: 10.0,
                      child: Container(
                        width: 23,
                        height: 23,
                        child: Center(
                          child: Icon(
                            Icons.directions_walk,
                            color: AppTheme.nearlyBlack,
                            size: 20,
                          ),
                        ),
                      ),
                    ),
                    Icon(
                      Icons.chevron_right,
                      color: AppTheme.white,
                      size: 20,
                    ),
                  ],
                ),
              ])),
    );
  }
}
