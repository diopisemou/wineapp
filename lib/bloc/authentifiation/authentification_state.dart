part of 'authentification_bloc.dart';

@immutable
abstract class AuthentificationState {}

class AuthentificationInitial extends AuthentificationState {}

class AuthenticationUninitialized extends AuthentificationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthentificationState {
  @override
  String toString() => 'AuthenticationAuthenticated';
}

class AuthenticationUnauthenticated extends AuthentificationState {
  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthentificationState {
  @override
  String toString() => 'AuthenticationLoading';
}
