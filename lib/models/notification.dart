class MyNotification {
  final String id;
  final DateTime date;
  final String title;
  final String message;
  final bool visited;

  MyNotification({
    this.id,
    this.date,
    this.title,
    this.message,
    this.visited,
  });

  MyNotification copyWith({
    String id,
    DateTime date,
    String title,
    String message,
    bool visited,
  }) {
    return MyNotification(
      id: id ?? this.id,
      date: date ?? this.date,
      title: title ?? this.title,
      message: message ?? this.message,
      visited: visited ?? this.visited,
    );
  }
}
