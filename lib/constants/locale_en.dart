const dynamic text_local = {
  'APP_WELCOME' : "Bienvenue",
  'APP_WELCOME_TEXT' : "Planifiee ou improvisee :  organisew votre sortie selon vos desirs et vos besoins",
  'EMAIL' : "Email",
  'PASSWORD' : "Mot de passe",
  'CONNEXION' : "Connexion",
  'REGISTER' : "S'enregister",
  'FACEBOOK_CONNEXION' : "Connexion avec facebook",
  'EMAIL_HINT' : "Your email address",
  'PASSWORD_HINT' : "Your password",
  'EMPTY_EMAIL' : "Email can't be empty",
  'EMPTY_PASSWORD' : "Password can't be empty",
  'CONTINUE' : "Continue",
};