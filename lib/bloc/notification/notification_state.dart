part of 'notification_bloc.dart';

abstract class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object> get props => [];
}

class NotificationInitial extends NotificationState {
  @override
  String toString() => "NotificationInitial";
}

class NotificationLoaded extends NotificationState {
  final List<MyNotification> notifications;

  NotificationLoaded(this.notifications);
  @override
  List<Object> get props => [notifications];
  @override
  String toString() => 'NotificationLoaded(notifications: $notifications)';
}
