import 'package:flutter/material.dart';
import 'package:wineapp/helpers/flutter_login/src/widgets/icon_badge.dart';
import 'package:wineapp/pages/home_tab.dart';


class MainScreenOld extends StatefulWidget {
  @override
  _MainScreenOldState createState() => _MainScreenOldState();
}

class _MainScreenOldState extends State<MainScreenOld> {
  PageController _pageController;
  int _page = 0;

  @override
  Widget build(BuildContext context) {

    Widget barIcon(
        {IconData icon = Icons.home, int page = 0, bool badge = false, String icontext = "home"}) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          IconButton(
            icon: badge ? IconBadge(icon: icon, size: 24) : Icon(icon, size: 24),
            color: _page == page
                ? Theme.of(context).accentColor
                : Colors.blueGrey[300],
            onPressed: () => _pageController.jumpToPage(page),
          ),
          Text(
            icontext,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 10,
              ),
              maxLines: 1,
              textAlign: TextAlign.center,
              ),
        ],
      );
    }

    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: onPageChanged,
        children: List.generate(4, (index) => HomeTab()),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 7),
            barIcon(icon: Icons.home, page: 0),
            barIcon(icon: Icons.favorite, page: 1),
            barIcon(icon: Icons.mode_comment, page: 2, badge: true),
            barIcon(icon: Icons.person, page: 3),
            SizedBox(width: 7),
          ],
        ),
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}