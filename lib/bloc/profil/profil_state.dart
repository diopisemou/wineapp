part of 'profil_bloc.dart';


abstract class ProfilState extends Equatable {
  const ProfilState();

  @override
  List<Object> get props => [];
}

class ProfilInitial extends ProfilState {}

class ProfilUpdating extends ProfilState {
  @override
  String toString() => 'Profil is updating';
}

class ProfilDisplayed extends ProfilState {
  final String fullName;
  final String email;

  ProfilDisplayed({this.fullName, this.email});

  @override
  String toString() => 'ProfilDispladed(fullName: $fullName, email: $email)';

  @override
  List<Object> get props => [this.fullName, this.email];
}



class ProfilUpdated extends ProfilState {
  final String message;
  final UpdateStatus updateStatus;
  ProfilUpdated({this.message, this.updateStatus});
}

class ProfilPasswordState extends ProfilState {
  final bool isPasswordValid;
  final bool isIncorrectPassword;
  final bool formSubmittedSuccessfully;
  final String password;

  @override
  List<Object> get props => [
        this.isPasswordValid,
        this.formSubmittedSuccessfully,
        this.password,
        this.isIncorrectPassword
      ];

  ProfilPasswordState({
    this.isPasswordValid,
    this.formSubmittedSuccessfully,
    this.password,
    this.isIncorrectPassword,
  });

  ProfilPasswordState copyWith({
    bool isPasswordValid,
    bool isIncorrectPassword,
    bool formSubmittedSuccessfully,
    String password,
  }) {
    return ProfilPasswordState(
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isIncorrectPassword: isIncorrectPassword ?? this.isIncorrectPassword,
      formSubmittedSuccessfully:
          formSubmittedSuccessfully ?? this.formSubmittedSuccessfully,
      password: password ?? this.password,
    );
  }
}
