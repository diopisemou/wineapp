import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:wineapp/services/twilio_helper.dart';
import 'package:wineapp/constants/constants.dart';

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<String> signUpWithNumber(
      String email, String password, String mobilenumber);

  Future<User> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<Map> sendSMSRecoveryCode(String phoneNumber);

  Future<Map> sendSMSRecoveryWithCode(String phoneNumber, String verifCode);

  Future<void> sendSMSRecoveryCodeFireBase(String phoneNumber);

  Future<void> sendSMSRecoveryFireBaseCodeCallback(
      {String phoneNumber,
      Duration timeout,
      int forceResendingToken,
      void Function(AuthCredential) verificationCompleted,
      void Function(FirebaseAuthException) verificationFailed,
      void Function(String, [int]) codeSent,
      void Function(String) codeAutoRetrievalTimeout});

  Future<void> sendEmailRecoveryCode(String userEmail);

  Future<void> signOut();

  Future<bool> isEmailVerified();

  Future<dynamic> updatePassword(String _newPassword);
  Future<void> updteFullName(String newName);
  Future<void> updateEmail(String newEmail);
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  static final String _authToken = TWILIO_AUTH_TOKEN;
  static final String _accountSid = TWILIO_ACCOUNTS_ID;
  static final String _accountNumber = TWILLIO_ACCOUNT_NUMBER;
  final TwilioHelper _twilioHelper = new TwilioHelper(_accountSid, _authToken);

  Future<String> signIn(String email, String password) async {
    UserCredential result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<String> signUp(String email, String password) async {
    UserCredential result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<String> signUpWithNumber(
      String email, String password, String mobilenumber) async {
    UserCredential result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<User> getCurrentUser() async {
    return _firebaseAuth.currentUser;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    _firebaseAuth.currentUser.sendEmailVerification();
  }

  Future<Map> sendSMSRecoveryCode(String phoneNumber) async {
    // Send a text message
    var verifCode = '123456';
    Map messageResult = await _twilioHelper.messages.create({
      'body': 'Your Verification Code is : ' + verifCode,
      'from': _accountNumber, // a valid Twilio number
      'to': phoneNumber // your phone number
    });

    return messageResult;
  }

  Future<Map> sendSMSRecoveryWithCode(
      String phoneNumber, String verifCode) async {
    // Send a text message
    Map messageResult = await _twilioHelper.messages.create({
      'body': 'Your Verification Code is : ' + verifCode,
      'from': _accountNumber, // a valid Twilio number
      'to': phoneNumber // your phone number
    });

    return messageResult;
  }

  Future<void> sendSMSRecoveryCodeFireBase(String phoneNumber) async {
    _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: null,
        verificationCompleted: null,
        verificationFailed: null,
        codeSent: null,
        codeAutoRetrievalTimeout: null);
  }

  Future<void> sendSMSRecoveryFireBaseCodeCallback(
      {String phoneNumber,
      Duration timeout,
      int forceResendingToken,
      void Function(AuthCredential) verificationCompleted,
      void Function(FirebaseAuthException) verificationFailed,
      void Function(String, [int]) codeSent,
      void Function(String) codeAutoRetrievalTimeout}) async {
    _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: timeout,
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<void> sendEmailRecoveryCode(String userEmail) async {
    _firebaseAuth.sendPasswordResetEmail(email: userEmail);
  }

  Future<bool> isEmailVerified() async {
    return _firebaseAuth.currentUser.emailVerified;
  }

  Future<dynamic> updatePassword(String _newPassword) async {
    var resData = await _firebaseAuth.currentUser.updatePassword(_newPassword);

    return resData;
  }

  @override
  Future<void> updateEmail(String newEmail) {}

  @override
  Future<void> updteFullName(String newName) {}
}
