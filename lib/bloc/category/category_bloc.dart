import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:wineapp/models/category.dart';
import 'package:wineapp/services/category_service.dart';
import '../shared.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategorieEvent, CategoryState> {
  final CategoryService _categoryService;
  StreamSubscription _categoriesSubscription;
  StreamSubscription _favoriteCategoriesSubscription;
  CategoryBloc({@required CategoryService categoryService})
      : assert(categoryService != null),
        _categoryService = categoryService,
        super(CategorieInitial());

  @override
  Stream<CategoryState> mapEventToState(
    CategorieEvent event,
  ) async* {
    if (event is LoadCategories) {
      yield* _mapLoadCatetoriesToState();
    } else if (event is LoadFavoriteCategories) {
      yield* _mapLoadFavoriteCategoriesToState();
    } else if (event is LoadCategoriesForChoice) {
      yield* _mapLoadCategoriesForChoose(event);
    } else if (event is CategoriesUpdated) {
      yield* _mapCategoriesUpdatedToState(event);
    } else if (event is FavoriteCategoriesUpdated) {
      yield* _mapFavoriteCategoriesUpdatedToState(event);
    } else if (event is ToogleFavorite) {
      yield* _mapToogleFavoriteToState(event);
    } else if (event is LongPressFavoriteCategory) {
      yield* _mapLongPressFavoriteCategoryToState(event);
    } else if (event is UpdateFavoriteCategories) {
      yield* _mapUpdateFavoriteCategoriesToState(event);
    }
  }

  Stream<CategoryState> _mapLoadCatetoriesToState() async* {
    _categoriesSubscription?.cancel();
    _categoriesSubscription = _categoryService
        .categories()
        .listen((categories) => add(CategoriesUpdated(categories)));
  }

  Stream<CategoryState> _mapLoadFavoriteCategoriesToState() async* {
    _favoriteCategoriesSubscription?.cancel();
    _favoriteCategoriesSubscription = _categoryService
        .favoriteCategories()
        .listen((categories) => add(FavoriteCategoriesUpdated(categories)));
  }

  Stream<CategoryState> _mapCategoriesUpdatedToState(
      CategoriesUpdated event) async* {
    yield CategoryLoaded(event.categories);
  }

  Stream<CategoryState> _mapFavoriteCategoriesUpdatedToState(
      FavoriteCategoriesUpdated event) async* {
    yield FavoriteCategoriesLoaded(event.categories
        .map((e) => CategoryForChooseFavorite(category: e))
        .toList());
  }

  Stream<CategoryState> _mapToogleFavoriteToState(ToogleFavorite event) async* {
    final categoriesChoose = event.categoriesForChoose
        .map<CategoryForChooseFavorite>((e) =>
            e.category.key == event.categoryForChooseFavorite.category.key
                ? event.categoryForChooseFavorite
                : e)
        .toList();
    yield CategoryLoadedForChoiceFavorite(categoriesChoose);
  }

  Stream<CategoryState> _mapLongPressFavoriteCategoryToState(
      LongPressFavoriteCategory event) async* {
    final favoriteCategories = event.favoritecategoriesForChoice
        .map<CategoryForChooseFavorite>((e) =>
            e.category.key == event.pressedFavorite.category.key
                ? event.pressedFavorite
                : e)
        .toList();
    yield FavoriteCategoriesLoaded(favoriteCategories);
  }

  bool _isFavorite(List<Category> categories, Category category) {
    final res = categories.firstWhere((element) => element.key == category.key,
        orElse: () => null);
    return res != null;
  }

  // pour le chargement du modal choix de category favoris
  Stream<CategoryState> _mapLoadCategoriesForChoose(
      LoadCategoriesForChoice event) async* {
        
    final categoriesFavorite =
        await _categoryService.favoriteCategories().first;
    final categories = await _categoryService.categories().first;

    final categoriesChoose = categories
        .map((e) => CategoryForChooseFavorite(
            category: e, isFavorite: _isFavorite(categoriesFavorite, e)))
        .toList();
    yield CategoryLoadedForChoiceFavorite(categoriesChoose);
  }

  Stream<CategoryState> _mapUpdateFavoriteCategoriesToState(
      UpdateFavoriteCategories event) async* {
    try {
      await _categoryService.updateFavoriteCategories(event.categories);
     yield AfterFavoriteCategoriesUpdated(
          updateStatus: UpdateStatus.success,
          message: 'category favoris mis à jour avec succès');
    } catch (e) {
      yield AfterFavoriteCategoriesUpdated(
          updateStatus: UpdateStatus.failed,
          message: 'erreur lors de la mise à jour');
    }
  }

  @override
  Future<void> close() {
    _categoriesSubscription?.cancel();
    _favoriteCategoriesSubscription?.cancel();
    return super.close();
  }
}
