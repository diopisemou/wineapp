import 'package:flutter/material.dart';
import 'package:wineapp/widgets/custom_button_edit_profil.dart';

class FormEmail extends StatefulWidget {
  final Function(String email) onSave;
  final String currentValue;
  FormEmail({Key key, this.onSave, @required this.currentValue})
      : super(key: key);

  @override
  _FormEmailState createState() => _FormEmailState();
}

class _FormEmailState extends State<FormEmail> {
  final _keyFormEmail = GlobalKey<FormState>();
  String _newEmail;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _keyFormEmail,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10),
          Text(
            'Saisir votre email',
            style: TextStyle(fontSize: 17),
          ),
          TextFormField(
            initialValue: widget.currentValue,
            onSaved: (value) => _newEmail = value,
            validator: (value) {
              return value.isEmpty ? 'email non invalide' : null;
            },
          ),
          CustomButtomEditProfil(
            formKey: _keyFormEmail,
            onSave: () => widget.onSave(_newEmail),
          ),
         // SizedBox(height: MediaQuery.of(context).viewInsets.bottom + 10),
        ],
      ),
    );
  }
}
